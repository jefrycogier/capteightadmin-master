@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Menu
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-tags"></i>Menu</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
            <a style="float: right;" href="/menu/add/" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus mr-5"></i>Tambah Menu</a>
        </div>
        <div class="box-body table-responsive">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <table id="menu-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>    
                            <th>Gambar</th>
                            <th>Nama</th>
                            <th>Kategori Menu</th>
                            <th>Dibuat</th>
                            <th>Dibuat Oleh</th>
                            <th style="width: 120px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($menu as $p)
                        <tr>
                            <td><img class="img-responsive" src="{{ $p->image_url }}"  alt="Image" style="border-radius: 10px; max-width: 50px;"></td>
                            <td>{{ $p->name }}</td>
                            <td>{{ $p->category_name }}</td>
                            <td>{{ date('d-m-Y H:i:s', strtotime($p->created_at)) }}</td>
                            <td>{{ $p->created_by }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-default" data-id="{{ $p->menu_id }}" style="margin-right: 5px;"><i class="fa fa-bars"></i></button>
                                    <a href="/menu/edit/{{$p->menu_id}}" style="margin-right: 5px;" type="button" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="/menu/delete/{{$p->menu_id}}" onclick="return confirm('Apakah anda yakin ?')" type="button" id="btn-confirm" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Detail</h4>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <div class="dt-padding">
                            <dt>Gambar Menu</dt>
                            <dd><img class="img-responsive" src="" alt="Image" id="menu_image" style="border-radius:5px; max-width: 400px;"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Nama Menu</dt>
                            <dd id="menu_name"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Kategori Menu</dt>
                            <dd id="menu_category"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Dibuat</dt>
                            <dd id="created_at"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Dibuat Oleh</dt>
                            <dd id="created_by"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Diperbarui</dt>
                            <dd id="updated_at"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Diperbarui Oleh</dt>
                            <dd id="updated_by"></dd>
                        </div>
                    </dl>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection

@section('jquery')
<script>
    $(function () {
        $('#menu-table').DataTable()
    });
    function myFunction(menuId) {
        var dots = document.getElementById("dots" + menuId);
        var moreText = document.getElementById("more" + menuId);
        var btnText = document.getElementById("myBtn" + menuId);
        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    }

    $('#detail').off('click').on('click', function () {
        $('#modalMdContent').load($(this).attr('value'));
        $('#modalMdTitle').html($(this).attr('title'));
    });
    $(function () {
        $('#modal-default').on("show.bs.modal", function (e) {
            var id = $(e.relatedTarget).data('id')
            $.ajax({
                type: 'POST',
                url: '/menu/detail',
                data: {"_token": "{{ csrf_token() }}", menu_id: id},
                success: function (data) {
                    $("#menu_image").attr("src", data.menu_image);
                    $('#menu_name').html(data.menu_name);
                    $('#menu_category').html(data.menu_category);
                    $('#created_at').html(data.created_at);
                    $('#created_by').html(data.created_by);
                    $('#updated_at').html(data.updated_at);
                    $('#updated_by').html(data.updated_by);
                }
            });
        });
    });
</script>
@endsection