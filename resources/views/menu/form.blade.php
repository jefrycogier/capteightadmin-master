@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        {{ $status_title }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="/menu"><i class="fa fa-tags"></i>Menu</a></li>
        <li class="active"><a href="#"><i class="{{ $status_icon }}"></i>{{ $status_title }}</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/menu/save">
                    {{ csrf_field() }}

                    <input type="hidden" name="menu_id" value="{{$menu_id}}">

                    <div class="form-group">    
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <img id="image-preview" class="img-responsive" src="{{ $image_url }}" style="max-width: 370px; border-radius: 5px;">
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('image_src') ? ' has-error' : '' }}">
                        <label for="inputImage" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" id="image-selector" name="file" accept="image/*">
                            @if ($errors->has('image_src'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image_src') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <textarea style="display: none;" class="form-control" name="image_src" id="image_src">{{ $image_name }}</textarea>

                    <div class="form-group">
                        <label for="menu_category_id" class="col-sm-2 control-label">Kategori Menu</label>
                        <div class="col-sm-4">
                            <select class="form-control select2" id="menu_category_id" name="menu_category_id">
                                @foreach($menu_category_list as $row)
                                <option value="<?php echo $row["menu_category_id"] ?>"><?php echo $row["name"]; ?></option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{{old('name', $name)}}" required autofocus>
                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-sm btn-primary btn-flat pull-right">Submit</button>
                <a href="/menu/" type="button" class="btn btn-sm btn-flat btn-danger pull-right mr-5"></i>Cancel</a>
            </div>
            </form>

            <div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-name">Cropping Image</h4>
                        </div>
                        <div class="modal-body">
                            <div class="image-container">
                                <img id="image" name="image">
                                <input type="hidden" id="cropped-image" name="cropped-image">
                            </div>
                            <label class="btn">Zoom
                                <span id="dataZoom">100</span>%
                            </label>
                            <div id="zoom-slider"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">Batal</button>
                            <button type="submit" id="upload_image" class="btn btn-primary btn-sm btn-flat">Upload Gambar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('jquery')
<script>
    $("#menu_category_id").select2({
        allowClear: false
    });
    $("#menu_category_id").val('').trigger('change');
<?php if (strlen($menu_category_id) > 0) { ?>
        $(function () {
            var category = "<?php echo $menu_category_id; ?>";
            $('#menu_category_id').val(category);
            $('#menu_category_id').select2().trigger('change');
        });
<?php } ?>

    //image
    var avatar = document.getElementById('image-preview');
    var $image = $('#image');
    var fileType;
    var fileData;
    $('#loadingDiv').hide();
    $(document)
            .ajaxStart(function () {
                $('#loadingDiv').show();
            })
            .ajaxStop(function () {
                $('#loadingDiv').hide();
            });
    $('#image-selector').change(function (event) {
        var imageTypes = ["image/gif", "image/jpeg", "image/png"];
        fileData = this.files[0];
        fileType = this.files[0].type;
        if ($.inArray(fileType, imageTypes) > 0) {
            if (this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $image.attr('src', e.target.result).hide();
                };
                reader.readAsDataURL(this.files[0]);
                $('#modal').modal('show');
            }
        } else {
            alert("Format tidak bisa di crop !");
        }
    });
    $('#modal').on('shown.bs.modal', function () {
        $image.show();
        $image.cropper({
            viewMode: 0,
            dragMode: 'move',
            aspectRatio: 5 / 5,
            restore: false,
            guides: false, highlight: false,
            cropBoxMovable: false,
            cropBoxResizable: false,
            zoom: function (e) {
                if (e.ratio > 10 || e.ratio < 0.01) {
                    e.preventDefault();
                } else {
                    var ratio = Math.round(e.ratio * 1000) / 10;
                    $('#dataZoom').text(ratio);
                    console.log(typeof e.originalEvent);
                    if (typeof e.originalEvent !== "undefined") {
                        $('#zoom-slider').slider("value", e.ratio);
                    }
                }
            }, ready: function (e) {
                $image.cropper('zoomTo', 1);
            }});
    });
    $('#zoom-slider').slider({
        min: 0.01, max: 10,
        value: 1,
        step: 0.01, slide: function (event, ui) {
            if ($image.data('cropper')) {
                $image.cropper('zoomTo', ui.value);
            }
        }
    });
    $('#modal').on('hidden.bs.modal', function () {
        $image.cropper('destroy');
        //        $('#image-selector').val("");
        $('#zoom-slider').slider("value", 1);
    });
    document.getElementById('upload_image').addEventListener('click', function (e) {
        e.preventDefault();
        var formData = new FormData();
        var canvasImage;
        if (fileType == "image/png") {
            canvasImage = $image.cropper('getCroppedCanvas', {
                width: 500,
                height: 500
            });
        } else {
            canvasImage = $image.cropper('getCroppedCanvas', {fillColor: '#fff',
                width: 500,
                height: 500});
        }

        var croppedImage = canvasImage.toDataURL(fileType);
        avatar.src = canvasImage.toDataURL();
        $("#image_src").val(canvasImage.toDataURL());
        formData.append('croppie', croppedImage);
        formData.append('action', "uploadimage");
        $.ajax({
            type: 'GET',
            url: $('#form').attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log("success");
                console.log(data);
                $('#modal').modal('hide');
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });
</script>
@endsection