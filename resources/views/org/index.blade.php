@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Setting
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-cogs"></i>Setting</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <form class="form-horizontal" method="POST" action="/org/save" enctype="multipart/form-data">
                    {{ csrf_field() }} 

                    <input type="hidden" name="org_id" value="{{$org_id}}">

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <img id="image-preview" class="img-responsive" style="max-width: 500px; border-radius: 10px;" src="{{ $image_url }}">
                        </div>
                    </div>

                    <div class="form-group hidden">
                        <label for="inputImage" class="col-sm-2 control-label">Mini Banner</label>
                        <div class="col-sm-10">
                            <input type="file" id="image-selector" name="file" accept="image/*">
                        </div>
                    </div>

                    <textarea style="display: none;" class="form-control" name="image_src" id="image_src">{{ $image_name }}</textarea>

                    <div class="form-group">
                        <label for="onoff" class="col-sm-2 control-label">Maintenance Mode</label>
                        <div class="col-sm-10">
                            <label class="switch"><input type="checkbox" id="togBtn" name="switchButton" <?php echo $statusWeb ?>>
                                <div class="slider round">
                                    <span class="on"></span>
                                    <span class="off"></span>
                                </div>
                            </label>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-2 control-label">Nama Aplikasi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name', $name)}}" required>
                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('domain_backend') ? ' has-error' : '' }}">
                        <label for="domain_backend" class="col-sm-2 control-label">Domain Backend</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="domain_backend" name="domain_backend" value="{{old('domain_backend', $domain_backend)}}" required>
                            @if ($errors->has('domain_backend'))
                            <span class="help-block">
                                <strong>{{ $errors->first('domain_backend') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('domain_frontend') ? ' has-error' : '' }}">
                        <label for="domain_frontend" class="col-sm-2 control-label">Domain Frontend</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="domain_frontend" name="domain_frontend" value="{{old('domain_frontend', $domain_frontend)}}" required>
                            @if ($errors->has('domain_frontend'))
                            <span class="help-block">
                                <strong>{{ $errors->first('domain_frontend') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('domain_mail') ? ' has-error' : '' }}">
                        <label for="domain_mail" class="col-sm-2 control-label">Domain Mail</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="domain_mail" name="domain_mail" value="{{old('domain_mail', $domain_mail)}}" required>
                            @if ($errors->has('domain_mail'))
                            <span class="help-block">
                                <strong>{{ $errors->first('domain_mail') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('domain_cpanel') ? ' has-error' : '' }}">
                        <label for="domain_cpanel" class="col-sm-2 control-label">Domain Cpanel</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="domain_cpanel" name="domain_cpanel" value="{{old('domain_cpanel', $domain_cpanel)}}" required>
                            @if ($errors->has('domain_cpanel'))
                            <span class="help-block">
                                <strong>{{ $errors->first('domain_cpanel') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="domain_cpanel" class="col-sm-2 control-label">Manual Book</label>
                        <div class="col-sm-10">
                            <input type="file" name="file"><a href="{{ $file_url }}" target="_blank">{{ $file_name }}</a>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-primary pull-right btn-flat">Update</button>
                    </div>

                </form>

                <div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Cropping Image</h4>
                            </div>
                            <div class="modal-body">
                                <div class="image-container">
                                    <img id="image" name="image">
                                    <input type="hidden" id="cropped-image" name="cropped-image">
                                </div>
                                <label class="btn">Zoom
                                    <span id="dataZoom">100</span>%
                                </label>
                                <div id="zoom-slider"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">Batal</button>
                                <button type="submit" id="upload_image" class="btn btn-primary btn-sm btn-flat">Upload Gambar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cropping Image</h4>
                </div>
                <div class="modal-body">
                    <div class="image-container">
                        <img id="image" name="image">
                        <input type="hidden" id="cropped-image" name="cropped-image">
                    </div>
                    <label class="btn">Zoom
                        <span id="dataZoom">100</span>%
                    </label>
                    <div id="zoom-slider"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">Batal</button>
                    <button type="submit" id="upload_image" class="btn btn-primary btn-sm btn-flat">Upload Gambar</button>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


@section('jquery')
<script>

    var isAdmin = "<?php echo Auth::user()->is_admin ?>";
    if (isAdmin == 0) {
        $("#domain_backend").prop('readonly', true);
        $("#domain_frontend").prop('readonly', true);
        $("#domain_mail").prop('readonly', true);
        $("#domain_cpanel").prop('readonly', true);
        $("#name").prop('readonly', true);
    }

</script>
<script type="text/javascript">
    var avatar = document.getElementById('image-preview');
    var $image = $('#image');
    var fileType;
    var fileData;

    $('#loadingDiv').hide();

    $(document)
            .ajaxStart(function () {
                $('#loadingDiv').show();
            })
            .ajaxStop(function () {
                $('#loadingDiv').hide();
            });

    $('#image-selector').change(function (event) {
        var imageTypes = ["image/gif", "image/jpeg", "image/png"];
        fileData = this.files[0];
        fileType = this.files[0].type;

        if ($.inArray(fileType, imageTypes) > 0) {
            if (this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $image.attr('src', e.target.result).hide();
                };
                reader.readAsDataURL(this.files[0]);
                $('#modal').modal('show');
            }
        } else {
            alert("Format tidak bisa di crop !");
        }
    });

    $('#modal').on('shown.bs.modal', function () {
        $image.show();
        $image.cropper({
            viewMode: 0,
            dragMode: 'move',
            aspectRatio: 16 / 4.5,
            restore: false,
            guides: false,
            highlight: false,
            cropBoxMovable: false,
            cropBoxResizable: false,
            zoom: function (e) {
                if (e.ratio > 10 || e.ratio < 0.01) {
                    e.preventDefault();
                } else {
                    var ratio = Math.round(e.ratio * 1000) / 10;
                    $('#dataZoom').text(ratio);
                    if (typeof e.originalEvent !== "undefined") {
                        $('#zoom-slider').slider("value", e.ratio);
                    }
                }
            },
            ready: function (e) {
                $image.cropper('zoomTo', 1);
            }
        });

    });

    $('#zoom-slider').slider({
        min: 0.01,
        max: 10,
        value: 1,
        step: 0.01,
        slide: function (event, ui) {
            if ($image.data('cropper')) {
                $image.cropper('zoomTo', ui.value);
            }
        }
    });

    $('#modal').on('hidden.bs.modal', function () {
        $image.cropper('destroy');
//        $('#image-selector').val("");
        $('#zoom-slider').slider("value", 1);
    });

    document.getElementById('upload_image').addEventListener('click', function (e) {
        e.preventDefault();
        var formData = new FormData();
        var canvasImage;
        if (fileType == "image/png") {
            canvasImage = $image.cropper('getCroppedCanvas', {
                width: 2274,
                height: 600
            });
        } else {
            canvasImage = $image.cropper('getCroppedCanvas', {
                fillColor: '#fff',
                width: 2274,
                height: 600
            });
        }

        var croppedImage = canvasImage.toDataURL(fileType);
        avatar.src = canvasImage.toDataURL();
        $("#image_src").val(canvasImage.toDataURL());
        formData.append('croppie', croppedImage);
        formData.append('action', "uploadimage");

        $.ajax({
            type: 'GET',
            url: $('#form').attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log("success");
                $('#modal').modal('hide');
            },
            error: function (data) {
                console.log("error");
            }
        });
    });
</script>

@endsection