@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Users
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-users"></i>Users</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title">Data</h3><a style="float: right;" href="/users/add/" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus mr-5"></i>Tambah User</a>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <div class="box-body table-responsive">
                    <table id="users-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 100px">Gambar</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Dibuat</th>
                                <th>Dibuat Oleh</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $p)
                            <?php
                            $image = '/adminlte/img/avatar5.png';
                            if ($p->image_url != NULL) {
                                $image = $p->image_url;
                            }
                            ?>
                            <tr>
                                <td><img class="img-responsive" src="{{$image}}" alt="Image" style="border-radius:5px; max-width: 50px;"></td>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->username }}</td>
                                <td>{{ $p->email }}</td>
                                <td>{{ date('d-m-Y', strtotime($p->created_at)) }}</td>
                                <td>{{ $p->created_by }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-default" data-id="{{ $p->id }}" style="margin-right: 5px;"><i class="fa fa-bars"></i></button>
                                        <a href="/users/edit/{{$p->id}}" style="margin-right: 5px;" type="button" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="/users/delete/{{$p->id}}" onclick="return confirm('Apakah anda yakin ?')" type="button" id="btn-confirm" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTitle">Detail</h4>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <div class="dt-padding">
                            <dt>Gambar</dt>
                            <dd><img class="img-responsive" src="" id="image_url" style="border-radius:15px; max-width: 100px;"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Username</dt>
                            <dd id="username"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Nama</dt>
                            <dd id="name"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Jabatan</dt>
                            <dd id="roles"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Email</dt>
                            <dd id="email"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Dibuat</dt>
                            <dd id="created_at"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Dibuat Oleh</dt>
                            <dd id="created_by"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Diperbarui</dt>
                            <dd id="updated_at"></dd>
                        </div>
                        <div class="dt-padding">
                            <dt>Diperbarui Oleh</dt>
                            <dd id="updated_by"></dd>
                        </div>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('jquery')
<script>
    $(function () {
        $('#users-table').DataTable()
    });

    $('#detail').off('click').on('click', function () {
        $('#modalMdContent').load($(this).attr('value'));
        $('#modalMdTitle').html($(this).attr('title'));
    });

    $(function () {
        $('#modal-default').on("show.bs.modal", function (e) {
            var id = $(e.relatedTarget).data('id')
            $.ajax({
                type: 'POST',
                url: '/users/detail',
                data: {"_token": "{{ csrf_token() }}", id: id},
                success: function (data) {
                    $("#image_url").attr("src", data.image_url);
                    $("#name").html(data.name);
                    $('#username').html(data.username);
                    $('#email').html(data.email);
                    $('#roles').html(data.roles);
                    $('#created_at').html(data.created_at);
                    $('#created_by').html(data.created_by);
                    $('#updated_at').html(data.updated_at);
                    $('#updated_by').html(data.updated_by);
                }
            });
        });
    });
</script>
@endsection