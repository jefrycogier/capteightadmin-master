<?php
$imgLogo = '/img/logo.jpg';
?>

@extends('layouts.app')

@section('content')

<div class="login-box">
    <div class="login-logo">
        <center><img style="max-width:200px; width: 100%; height: auto;" src="{{$imgLogo}}" alt=""></center>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Login</p>
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                <input type="username" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}" required autofocus>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" name="password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-info btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </form>
    </div>

</div>
@endsection