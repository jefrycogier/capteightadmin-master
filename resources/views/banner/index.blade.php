@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Banner
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-image"></i>Banner</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3><a style="float: right;" href="/banner/add/" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus mr-5"></i>Tambah Banner</a>
        </div>
        <div class="box-body table-responsive">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <table id="banner-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 200px;">Gambar</th>
                            <th>Dibuat</th>
                            <th>Dibuat Oleh</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($banner as $s)
                        <tr>
                            <td><img class="img-responsive" src="{{ $s->image_url }}"  alt="Image" style="border-radius: 10px; max-width: 200px;"></td>
                            <td>{{ date('d-m-Y H:i:s', strtotime($s->created_at)) }}</td>
                            <td>{{ $s->created_by }}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="/banner/edit/{{$s->banner_id}}" style="margin-right: 5px;" type="button" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="/banner/delete/{{$s->banner_id}}" onclick="return confirm('Apakah anda yakin ?')" type="button" id="btn-confirm" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

@endsection

@section('jquery')
<script>
    $(function () {
        $('#banner-table').DataTable()
    })
</script>
@endsection