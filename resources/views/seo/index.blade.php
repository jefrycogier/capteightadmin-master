@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Seo Optimize
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-search"></i>Seo Optimize</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <form class="form-horizontal" method="POST" action="/seo/save">
                    {{ csrf_field() }} 

                    <input type="hidden" name="seo_id" value="{{$seo_id}}">

                    <div class="form-group{{ $errors->has('site_name') ? ' has-error' : '' }}">
                        <label for="site_name" class="col-sm-2 control-label">Site Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="site_name" name="site_name" value="{{old('site_name', $site_name)}}">
                            @if ($errors->has('site_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('site_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('site_description') ? ' has-error' : '' }}">
                        <label for="site_description" class="col-sm-2 control-label">Site Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="site_description" name="site_description" value="{{old('site_description', $site_description)}}">
                            @if ($errors->has('site_description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('site_description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('site_keywords') ? ' has-error' : '' }}">
                        <label for="site_keywords" class="col-sm-2 control-label">Site Keywords</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="site_keywords" name="site_keywords" value="{{old('site_keywords', $site_keywords)}}">
                            @if ($errors->has('site_keywords'))
                            <span class="help-block">
                                <strong>{{ $errors->first('site_keywords') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('site_url') ? ' has-error' : '' }}">
                        <label for="site_url" class="col-sm-2 control-label">Site Url</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="site_keywords" name="site_url" value="{{old('site_url', $site_url)}}">
                            @if ($errors->has('site_url'))
                            <span class="help-block">
                                <strong>{{ $errors->first('site_url') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('google_analytic') ? ' has-error' : '' }}">
                        <label for="google_analytic" class="col-sm-2 control-label">Google Analytic</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="google_analytic" name="google_analytic" value="{{old('google_analytic', $google_analytic)}}">
                            @if ($errors->has('google_analytic'))
                            <span class="help-block">
                                <strong>{{ $errors->first('google_analytic') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-primary pull-right btn-flat">Update</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</section>

@endsection


@section('jquery')
<script>


</script>

@endsection