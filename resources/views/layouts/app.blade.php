<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel='icon' href='/img/favicon.png' type='image/x-icon'>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Login | Capt Eight</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/adminlte/bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="/adminlte/css/AdminLTE.min.css">
        <link rel="stylesheet" href="/adminlte/plugins/iCheck/square/blue.css">
        <link rel="stylesheet" href="/adminlte/css/admin.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    </head>
    <body class="hold-transition login-page" style="background-color: #f3e4d1!important;">
        <div id="app">
            @yield('content')
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/adminlte/plugins/iCheck/icheck.min.js"></script>
        <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
    });
});
        </script>
    </body>
</html>
