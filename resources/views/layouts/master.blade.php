<?php

use App\Http\Controllers\Controller;

$orgId = Controller::getOrgId();
$nameApps = "Capt Eight";
$imageProfile = '/adminlte/img/avatar5.png';
$logo = '/img/logo.jpg';
$logoSidebar = '/img/logo.jpg';
$favicon = '/img/logo.jpg';
$result = DB::table('users')->select(['image_url', 'image_name'])->where('id', Auth::user()->id)->first();
if ($result != NULL) {
    if ($result->image_url != NULL) {
        $imageProfile = $result->image_url;
    }
}
?>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                    <link rel='icon' href='{{$favicon}}' type='image/x-icon'>
                        <title>{{ Auth::user()->roles }} | {{ $nameApps }}</title>
                        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                            <link rel="stylesheet" href="/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
                                <link rel="stylesheet" href="/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
                                    <link rel="stylesheet" href="/adminlte/bower_components/Ionicons/css/ionicons.min.css">
                                        <link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
                                            <link rel="stylesheet" href="/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
                                                <link rel="stylesheet" href="/adminlte/css/AdminLTE.min.css">
                                                    <link rel="stylesheet" href="/adminlte/css/skins/_all-skins.min.css">
                                                        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
                                                            <link rel="stylesheet" href="/css/capteight.css">
                                                                <link rel="stylesheet" href="/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
                                                                    <link rel="stylesheet" href="/cropper/cropper.min.css">
                                                                        <link rel="stylesheet" href="/adminlte/css/jquery-ui.css">
                                                                            <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type='text/css'>

                                                                                </head>
                                                                                <body class="hold-transition skin-blue-light sidebar-mini">
                                                                                    <div class="wrapper">
                                                                                        <!--loading-->
                                                                                        <div id="overlay">
                                                                                            <div id="progstat"></div>
                                                                                            <div id="progress"></div>
                                                                                        </div>
                                                                                        <!--loading-->

                                                                                        <div class="preloader" style="display: none;">    
                                                                                            <svg width="200" height="200" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ripple" style="background:0 0">
                                                                                                <circle cx="50" cy="50" r="4.719" fill="none" stroke="#38c3bc" stroke-width="4">
                                                                                                    <animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="3" keySplines="0 0.2 0.8 1" begin="-1.5s" repeatCount="indefinite"/>
                                                                                                    <animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="3" keySplines="0.2 0 0.8 1" begin="-1.5s" repeatCount="indefinite"/>
                                                                                                </circle><circle cx="50" cy="50" r="27.591" fill="none" stroke="#38c3bc" stroke-width="4">
                                                                                                    <animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="3" keySplines="0 0.2 0.8 1" begin="0s" repeatCount="indefinite"/>
                                                                                                    <animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="3" keySplines="0.2 0 0.8 1" begin="0s" repeatCount="indefinite"/>
                                                                                                </circle>
                                                                                            </svg>
                                                                                        </div>

                                                                                        <header class="main-header">
                                                                                            <a href="{{$logoSidebar}}" class="logo">                
                                                                                                <span class="logo-mini"><img src="{{$favicon}}"></span>
                                                                                                <span class="logo-lg"><img src="{{$logoSidebar}}"></span>
                                                                                            </a>
                                                                                            <nav class="navbar navbar-static-top">
                                                                                                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                                                                                                    <span class="sr-only">Toggle navigation</span>
                                                                                                    <span class="icon-bar"></span>
                                                                                                    <span class="icon-bar"></span>
                                                                                                    <span class="icon-bar"></span>
                                                                                                </a>
                                                                                                <div class="navbar-custom-menu">
                                                                                                    <ul class="nav navbar-nav">
                                                                                                        <li class="dropdown user user-menu">
                                                                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                                                                <img src="{{$imageProfile}}" class="user-image" alt="User Image">
                                                                                                                    <span class="hidden-xs">{{ Auth::user()->name }}</span>
                                                                                                            </a>
                                                                                                            <ul class="dropdown-menu">
                                                                                                                <li class="user-header">
                                                                                                                    <img src="{{$imageProfile}}" class="img-circle" alt="User Image">
                                                                                                                        <p>
                                                                                                                            {{ Auth::user()->name }}
                                                                                                                        </p>
                                                                                                                        <div class="showing-roles">
                                                                                                                            <p>
                                                                                                                                {{ Auth::user()->roles }}
                                                                                                                            </p>
                                                                                                                        </div>
                                                                                                                </li>
                                                                                                                <li class="user-footer">
                                                                                                                    <div class="pull-right">
                                                                                                                        <a href="{{ route('logout') }}" class="btn btn-sm btn-danger btn-flat"
                                                                                                                           onclick="event.preventDefault();
                                                                                                                                   document.getElementById('logout-form').submit();">
                                                                                                                            <i class="fa fa-sign-out text-center"></i>
                                                                                                                            Logout
                                                                                                                        </a>
                                                                                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                                                                            {{ csrf_field() }}
                                                                                                                        </form>
                                                                                                                    </div>
                                                                                                                    <div class="pull-right">
                                                                                                                        <a href="/change_password" class="btn btn-sm btn-primary btn-flat mr-5">
                                                                                                                            <i class="fa fa-key text-center"></i>
                                                                                                                            Ganti Password
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </nav>
                                                                                        </header>

                                                                                        <aside class="main-sidebar">
                                                                                            <section class="sidebar">
                                                                                                <div class="user-panel" style="padding: 12px 5px 12px 15px;">
                                                                                                    <div class="pull-left image">
                                                                                                        <img src="{{$imageProfile}}" class="img-circle" alt="User Image">
                                                                                                    </div>
                                                                                                    <div class="pull-left info">
                                                                                                        <p>{{ Auth::user()->name }}</p>
                                                                                                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <ul class="sidebar-menu" data-widget="tree">
                                                                                                    <li class="header">MAIN NAVIGATION</li>
                                                                                                    <li><a href="/home"><i class="fa fa-home text-center"></i> <span>Dashboard</span></a></li>
                                                                                                    <li><a href="/banner"><i class="fa fa-image text-center"></i> <span>Banner</span></a></li>
                                                                                                    <li><a href="/about"><i class="fa fa-info text-center"></i> <span>About</span></a></li>
                                                                                                    <li><a href="/menu_category"><i class="fa fa-list text-center"></i> <span>Menu Category</span></a></li>
                                                                                                    <li><a href="/menu"><i class="fa fa-tags text-center"></i> <span>Menu</span></a></li>
                                                                                                    <li><a href="/brochure"><i class="fa fa-bullhorn text-center"></i> <span>Brochure</span></a></li>
                                                                                                    <li><a href="/contact"><i class="fa fa-map text-center"></i> <span>Lokasi</span></a></li>
                                                                                                    <li><a href="/social_media"><i class="fa fa-rss text-center"></i> <span>Social Media</span></a></li>
                                                                                                    <li><a href="/users"><i class="fa fa-user text-center"></i> <span>Users</span></a></li>
                                                                                                    <li><a href="/seo"><i class="fa fa-search text-center"></i> <span>Optimize Seo</span></a></li>
                                                                                                    <li><a href="/org"><i class="fa fa-cogs text-center"></i> <span>Setting</span></a></li>

                                                                                                </ul>
                                                                                            </section>
                                                                                        </aside>
                                                                                        <div class="content-wrapper">
                                                                                            @yield('content')
                                                                                        </div>

                                                                                        <footer class="main-footer">
                                                                                            <div class="pull-right hidden-xs">
                                                                                                <b>Version</b> 1.0
                                                                                            </div>
                                                                                            <strong>Copyright &copy;2020 <a href="#" target="_blank">{{ $nameApps }}</a>.</strong> All rights
                                                                                            reserved.
                                                                                        </footer>

                                                                                    </div>
                                                                                    <script src="/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
                                                                                    <script src="/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
                                                                                    <script src="/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
                                                                                    <script src="/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
                                                                                    <script src="/adminlte/js/adminlte.min.js"></script>
                                                                                    <script src="/adminlte/js/demo.js"></script>
                                                                                    <script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
                                                                                    <script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
                                                                                    <script src="/adminlte/js/jquery-ui.js"></script>
                                                                                    <script src="/cropper/cropper.min.js"></script>
                                                                                    <script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
                                                                                    <script src="/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
                                                                                    <script type="text/javascript">
                                                                                                                               (function () {
                                                                                                                                   function id(v) {
                                                                                                                                       return document.getElementById(v);
                                                                                                                                   }
                                                                                                                                   function loadbar() {
                                                                                                                                       var ovrl = id("overlay"),
                                                                                                                                               prog = id("progress"),
                                                                                                                                               stat = id("progstat"),
                                                                                                                                               img = document.images,
                                                                                                                                               c = 0;
                                                                                                                                       tot = img.length;

                                                                                                                                       function imgLoaded() {
                                                                                                                                           c += 1;
                                                                                                                                           var perc = ((100 / tot * c) << 0) + "%";
                                                                                                                                           prog.style.width = perc;
                                                                                                                                           stat.innerHTML = "Loading " + perc;
                                                                                                                                           if (c === tot)
                                                                                                                                               return doneLoading();
                                                                                                                                       }
                                                                                                                                       function doneLoading() {
                                                                                                                                           ovrl.style.opacity = 0;
                                                                                                                                           setTimeout(function () {
                                                                                                                                               ovrl.style.display = "none";
                                                                                                                                           }, 1200);
                                                                                                                                       }
                                                                                                                                       for (var i = 0; i < tot; i++) {
                                                                                                                                           var tImg = new Image();
                                                                                                                                           tImg.onload = imgLoaded;
                                                                                                                                           tImg.onerror = imgLoaded;
                                                                                                                                           tImg.src = img[i].src;
                                                                                                                                       }
                                                                                                                                   }
                                                                                                                                   document.addEventListener('DOMContentLoaded', loadbar, false);
                                                                                                                               }());

                                                                                                                               $(document).ready(function () {
                                                                                                                                   $('.sidebar-menu').tree()
                                                                                                                               })

                                                                                                                               $("form").submit(function () {
                                                                                                                                   $('.preloader').show();
                                                                                                                               });

                                                                                    </script>

                                                                                    @yield('jquery')
                                                                                </body>
                                                                                </html>
