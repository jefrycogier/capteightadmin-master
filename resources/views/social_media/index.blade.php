@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Social Media
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-feed"></i>Social Media</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <form class="form-horizontal" method="POST" action="/social_media/save">
                    {{ csrf_field() }}

                    <input type="hidden" name="social_media_id" value="{{$social_media_id}}">

                    <div class="form-group{{ $errors->has('url_ig') ? ' has-error' : '' }}">
                        <label for="url_ig" class="col-sm-2 control-label">Link Instagram</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="url_ig" value="{{old('url_ig', $url_ig)}}">
                            @if ($errors->has('url_ig'))
                            <span class="help-block">
                                <strong>{{ $errors->first('url_ig') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('url_fb') ? ' has-error' : '' }} hidden">
                        <label for="url_fb" class="col-sm-2 control-label">Link Facebook</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="url_fb" value="{{old('url_fb', $url_fb)}}">
                            @if ($errors->has('url_fb'))
                            <span class="help-block">
                                <strong>{{ $errors->first('url_fb') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="hidden form-group{{ $errors->has('url_twitter') ? ' has-error' : '' }}">
                        <label for="url_twitter" class="col-sm-2 control-label">Link Twitter</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="url_twitter" value="{{old('url_twitter', $url_twitter)}}">
                            @if ($errors->has('url_twitter'))
                            <span class="help-block">
                                <strong>{{ $errors->first('url_twitter') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('no_wa') ? ' has-error' : '' }}">
                        <label for="no_wa" class="col-sm-2 control-label">No Whatsapp</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="no_wa" value="{{old('no_wa', $no_wa)}}">
                            @if ($errors->has('no_wa'))
                            <span class="help-block">
                                <strong>{{ $errors->first('no_wa') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-primary btn-flat pull-right">Update</button>
                    </div>

                </form>

            </div>
        </div>

    </div>
</section>

@endsection