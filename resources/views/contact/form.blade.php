@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        {{ $status_title }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="/contact"><i class="fa fa-map"></i>Lokasi</a></li>
        <li class="active"><a href="#"><i class="{{ $status_icon }}"></i>{{ $status_title }}</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/contact/save">
                    {{ csrf_field() }}

                    <input type="hidden" name="contact_id" value="{{$contact_id}}">

                    <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
                        <label for="location_id" class="col-sm-2 control-label">Lokasi</label>
                        <div class="col-sm-4">
                            <select class="form-control select2" id="location_id" name="location_id">
                                @foreach($location_list as $row)
                                <option value="<?php echo $row["location_id"] ?>"><?php echo $row["name"]; ?></option>
                                @endforeach
                            </select>
                            @if ($errors->has('location_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('location_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="address" value="{{old('address', $address)}}" required autofocus>
                            @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-sm btn-primary btn-flat pull-right">Submit</button>
                <a href="/contact/" type="button" class="btn btn-sm btn-flat btn-danger pull-right mr-5"></i>Cancel</a>
            </div>
            </form>

        </div>
    </div>
</section>

@endsection

@section('jquery')
<script>
    $("#location_id").select2({
        allowClear: false
    });
    $("#location_id").val('').trigger('change');
<?php if (strlen($location_id) > 0) { ?>
        $(function () {
            var category = "<?php echo $location_id; ?>";
            $('#location_id').val(category);
            $('#location_id').select2().trigger('change');
        });
<?php } ?>

</script>
@endsection