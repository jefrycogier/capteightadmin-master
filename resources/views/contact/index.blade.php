@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Lokasi
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-map"></i>Lokasi</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
            <a style="float: right;" href="/contact/add/" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus mr-5"></i>Tambah Lokasi</a>
        </div>
        <div class="box-body table-responsive">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <table id="contact-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>    
                            <th>Lokasi</th>
                            <th>Alamat</th>
                            <th>Dibuat</th>
                            <th>Dibuat Oleh</th>
                            <th style="width: 120px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contact as $p)
                        <tr>
                            <td>{{ $p->location_name }}</td>
                            <td>{{ $p->address }}</td>
                            <td>{{ date('d-m-Y H:i:s', strtotime($p->created_at)) }}</td>
                            <td>{{ $p->created_by }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-default" data-id="{{ $p->contact_id }}" style="margin-right: 5px;"><i class="fa fa-bars"></i></button>
                                    <a href="/contact/edit/{{$p->contact_id}}" style="margin-right: 5px;" type="button" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="/contact/delete/{{$p->contact_id}}" onclick="return confirm('Apakah anda yakin ?')" type="button" id="btn-confirm" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</section>

@endsection

@section('jquery')
<script>
    $(function () {
        $('#contact-table').DataTable()
    });
</script>
@endsection