@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Brochure
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-bullhorn"></i>Brochure</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3><a style="float: right;" href="/brochure/add/" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus mr-5"></i>Tambah Brochure</a>
        </div>
        <div class="box-body table-responsive">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <table id="brochure-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 200px;">File</th>
                            <th>Nama Brochure</th>
                            <th>Type File</th>
                            <th>Dibuat</th>
                            <th>Dibuat Oleh</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($brochure as $s)
                        <tr>
                            <td><a href="{{ $s->file_url }}" target="_blank">{{ $s->file_name }}</a></td>
                            <td>{{ $s->name }}</td>
                            <td>
                                <?php if ($s->type_file == 'image') { ?>
                                    Gambar
                                <?php } else { ?>
                                    PDF
                                <?php } ?>
                            </td>
                            <td>{{ date('d-m-Y H:i:s', strtotime($s->created_at)) }}</td>
                            <td>{{ $s->created_by }}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="/brochure/edit/{{$s->brochure_id}}" style="margin-right: 5px;" type="button" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="/brochure/delete/{{$s->brochure_id}}" onclick="return confirm('Apakah anda yakin ?')" type="button" id="btn-confirm" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

@endsection

@section('jquery')
<script>
    $(function () {
        $('#brochure-table').DataTable();
    })
</script>
@endsection