@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        {{ $status_title }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="/brochure"><i class="fa fa-bullhorn"></i>Brochure</a></li>
        <li class="active"><a href="#"><i class="{{ $status_icon }}"></i>{{ $status_title }}</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body table-responsive">
            <div class="box-body pad">

                <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="/brochure/save">
                    {{ csrf_field() }}
                    <div class="box-body">

                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            Upload Validation Error<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <input type="hidden" name="brochure_id" value="{{$brochure_id}}">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="name" value="{{old('name', $name)}}" required autofocus>
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="type_file" class="col-sm-2 control-label">Type File</label>
                            <div class="col-sm-3">
                                <select class="form-control select2" id="type_file" name="type_file">
                                    <option value="image">Gambar</option>
                                    <option value="pdf">PDF</option>
                                </select>
                            </div>
                        </div>

                        <div id="div_image">
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <img id="image-preview" class="img-responsive" style="max-width: 300px; border-radius: 10px;" src="{{ $file_url }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputImage" class="col-sm-2 control-label">Gambar</label>
                                <div class="col-sm-10">
                                    <input type="file" id="image-selector" name="file" accept="image/*">
                                </div>
                            </div>

                            <textarea style="display: none;" class="form-control" name="image_src" id="image_src">{{ $file_name }}</textarea>
                        </div>

                        <div id="div_pdf">
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php if (strlen($file_url) > 0) { ?>
                                        <embed width="500" height="450" name="plugin" src="{{ $file_url }}" type="application/pdf">
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('file_pdf') ? ' has-error' : '' }}">
                                <label for="inputImage" class="col-sm-2 control-label">PDF</label>
                                <div class="col-sm-10">
                                    <input type="file" id="file_pdf" name="file_pdf" accept="application/pdf">
                                    @if ($errors->has('file_pdf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file_pdf') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-primary btn-flat pull-right">Submit</button>
                        <a href="/brochure/" type="button" class="btn btn-sm btn-flat btn-danger pull-right mr-5"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Cropping Image</h4>
                    </div>
                    <div class="modal-body">
                        <div class="image-container">
                            <img id="image" name="image">
                            <input type="hidden" id="cropped-image" name="cropped-image">
                        </div>
                        <label class="btn">Zoom
                            <span id="dataZoom">100</span>%
                        </label>
                        <div id="zoom-slider"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">Batal</button>
                        <button type="submit" id="upload_image" class="btn btn-primary btn-sm btn-flat">Upload Gambar</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('jquery')


<script type="text/javascript">
    $(function () {
        $('.select2').select2();
    });

    var type_file = "<?php echo $type_file; ?>";
    if (type_file.length > 0) {
        $('#type_file').val(type_file);
        $('#type_file').select2().trigger('change');
    }

    $('#type_file').on('change', function () {
        var value = this.value;
        if (value == 'image') {
            $("#div_pdf").hide();
            $("#div_image").show();
        } else {
            $("#div_image").hide();
            $("#div_pdf").show();
        }
    });

    $(document).ready(function () {
        var value = $('#type_file').val();
        if (value == 'image') {
            $("#div_pdf").hide();
            $("#div_image").show();
        } else {
            $("#div_image").hide();
            $("#div_pdf").show();
        }
    });


    var avatar = document.getElementById('image-preview');
    var $image = $('#image');
    var fileType;
    var fileData;

    $('#loadingDiv').hide();

    $(document)
            .ajaxStart(function () {
                $('#loadingDiv').show();
            })
            .ajaxStop(function () {
                $('#loadingDiv').hide();
            });

    $('#image-selector').change(function (event) {
        var imageTypes = ["image/gif", "image/jpeg", "image/png"];
        fileData = this.files[0];
        fileType = this.files[0].type;

        if ($.inArray(fileType, imageTypes) > 0) {
            if (this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $image.attr('src', e.target.result).hide();
                };
                reader.readAsDataURL(this.files[0]);
                $('#modal').modal('show');
            }
        } else {
            alert("Format tidak bisa di crop !");
        }
    });

    $('#modal').on('shown.bs.modal', function () {
        $image.show();
        $image.cropper({
            viewMode: 0,
            dragMode: 'move',
            aspectRatio: 4 / 6,
            restore: false,
            guides: false,
            highlight: false,
            cropBoxMovable: false,
            cropBoxResizable: false,
            zoom: function (e) {
                if (e.ratio > 10 || e.ratio < 0.01) {
                    e.preventDefault();
                } else {
                    var ratio = Math.round(e.ratio * 1000) / 10;
                    $('#dataZoom').text(ratio);
                    if (typeof e.originalEvent !== "undefined") {
                        $('#zoom-slider').slider("value", e.ratio);
                    }
                }
            },
            ready: function (e) {
                $image.cropper('zoomTo', 1);
            }
        });

    });

    $('#zoom-slider').slider({
        min: 0.01,
        max: 10,
        value: 1,
        step: 0.01,
        slide: function (event, ui) {
            if ($image.data('cropper')) {
                $image.cropper('zoomTo', ui.value);
            }
        }
    });

    $('#modal').on('hidden.bs.modal', function () {
        $image.cropper('destroy');
//        $('#image-selector').val("");
        $('#zoom-slider').slider("value", 1);
    });

    document.getElementById('upload_image').addEventListener('click', function (e) {
        e.preventDefault();
        var formData = new FormData();
        var canvasImage;
        if (fileType == "image/png") {
            canvasImage = $image.cropper('getCroppedCanvas', {
                width: 765,
                height: 1080
            });
        } else {
            canvasImage = $image.cropper('getCroppedCanvas', {
                fillColor: '#fff',
                width: 765,
                height: 1080
            });
        }

        var croppedImage = canvasImage.toDataURL(fileType);
        avatar.src = canvasImage.toDataURL();
        $("#image_src").val(canvasImage.toDataURL());
        formData.append('croppie', croppedImage);
        formData.append('action', "uploadimage");

        $.ajax({
            type: 'GET',
            url: $('#form').attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log("success");
                $('#modal').modal('hide');
            },
            error: function (data) {
                console.log("error");
            }
        });
    });
</script>

@endsection

