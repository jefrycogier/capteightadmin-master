@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Ganti Password
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-pencil"></i>Ganti Password</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <form class="form-horizontal" method="POST" action="/update_password">
                    {{ csrf_field() }}

                    <input type="hidden" name="id" value="{{ Auth::user()->id }}"> <br/>

                    <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                        <label for="current_password" class="col-sm-2 control-label">Password Lama</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control"  name="current_password" required>
                            @if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control"  name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation" class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-10">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div> 

                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-primary pull-right btn-flat">Update</button>
                        <a href="/home/" type="button" class="btn btn-sm btn-danger pull-right btn-flat mr-5"></i>Cancel</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>

@endsection