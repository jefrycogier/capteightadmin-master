@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-home"></i>Dashboard</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Tutorial</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <p class="small-box-title">Tutorial</p>
                                <p>Petunjuk penggunaan backend.</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-info"></i>
                            </div>
                            <a href="{{ $manual_book }}" target="_blank" class="small-box-footer">
                                Download <i class="fa fa-download"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <p class="small-box-title">Email</p>
                                <p>Mengarah ke email.</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <a href="{{ $domain_mail }}" target="_blank" class="small-box-footer">
                                Arahkan <i class="fa fa-share"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <p class="small-box-title">Cpanel</p>
                                <p>Mengarah ke cpanel.</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-cogs"></i>
                            </div>
                            <a href="{{ $domain_cpanel }}" target="_blank" class="small-box-footer">
                                Pengaturan <i class="fa fa-cog"></i>
                            </a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

</section>

@endsection

@section('jquery')
<script>

</script>
@endsection