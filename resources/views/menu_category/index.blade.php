@extends('layouts.master') @section('content')

<section class="content-header">
    <h1>
        Jenis Menu
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-list-alt"></i>Kategori Menu</a></li>
    </ol>
</section>

<section class="content">

    @if ($error > 0)
    <div class="alert alert-danger">
        Error
        <ul>
            <li>{{ $errorMessage }}</li>
        </ul>
    </div>
    @endif

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
            <?php if ($can_add) { ?>
                <a style="float: right;" href="/menu_category/add/" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus mr-5"></i>Tambah Jenis Menu</a>
            <?php } ?>
        </div>
        <div class="box-body table-responsive">

            @include('layouts.flash_message')

            <div class="dd nestable">
                <ol class="dd-list">
                    <?php echo $categoriesTree ?>
                </ol>
            </div>
        </div>
    </div>

</section>
@endsection @section('jquery')
<script>
    $(function () {
        $('#menu_category-table').DataTable();
    })
</script>
@endsection