@extends('layouts.master')
@section('content')

<section class="content-header">
    <h1>
        About
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-info"></i>About</a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
        </div>
        <div class="box-body">
            <div class="box-body pad">

                @include('layouts.flash_message')

                <form class="form-horizontal" method="POST" action="/about/save">
                    {{ csrf_field() }} 

                    <input type="hidden" name="about_id" value="{{$about_id}}">

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <img id="image-preview" class="img-responsive" src="{{ $image_url }}" style="max-width: 150px; border-radius: 20px;">
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('image_src') ? ' has-error' : '' }}" style="display: none;">
                        <label for="inputImage" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" id="image-selector" name="file" accept="image/*">
                            @if ($errors->has('image_src'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image_src') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <textarea style="display: none;" class="form-control" name="image_src" id="image_src">{{ $image_name }}</textarea>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-sm-2 control-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea class="textarea" id="description-textarea" placeholder="" name="description"
                                      style="outline: none;width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                {{old('description', $description)}}
                            </textarea>
                            @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-primary btn-flat pull-right">Update</button>
                    </div>

                </form>

                <div class="modal fade" id="modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Cropping Image</h4>
                            </div>
                            <div class="modal-body">
                                <div class="image-container">
                                    <img id="image" name="image">
                                    <input type="hidden" id="cropped-image" name="cropped-image">
                                </div>
                                <label class="btn">Zoom
                                    <span id="dataZoom">100</span>%
                                </label>
                                <div id="zoom-slider"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm btn-flat" data-dismiss="modal">Batal</button>
                                <button type="submit" id="upload_image" class="btn btn-primary btn-sm btn-flat">Upload Gambar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>                    
        </div>
    </div>
</section>

@endsection

@section('jquery')
<script>
    $(function () {
        $('#description-textarea').wysihtml5();
    });

    var avatar = document.getElementById('image-preview');
    var $image = $('#image');
    var fileType;
    var fileData;

    $('#loadingDiv').hide();

    $(document)
            .ajaxStart(function () {
                $('#loadingDiv').show();
            })
            .ajaxStop(function () {
                $('#loadingDiv').hide();
            });

    $('#image-selector').change(function (event) {
        var imageTypes = ["image/gif", "image/jpeg", "image/png"];
        fileData = this.files[0];
        fileType = this.files[0].type;

        if ($.inArray(fileType, imageTypes) > 0) {
            if (this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $image.attr('src', e.target.result).hide();
                };
                reader.readAsDataURL(this.files[0]);
                $('#modal').modal('show');
            }
        } else {
            alert("Format tidak bisa di crop !");
        }
    });

    $('#modal').on('shown.bs.modal', function () {
        $image.show();
        $image.cropper({
            viewMode: 0,
            dragMode: 'move',
            aspectRatio: 7 / 8,
            restore: false,
            guides: false,
            highlight: false,
            cropBoxMovable: false,
            cropBoxResizable: false,
            zoom: function (e) {
                if (e.ratio > 10 || e.ratio < 0.01) {
                    e.preventDefault();
                } else {
                    var ratio = Math.round(e.ratio * 1000) / 10;
                    $('#dataZoom').text(ratio);
                    console.log(typeof e.originalEvent);
                    if (typeof e.originalEvent !== "undefined") {
                        $('#zoom-slider').slider("value", e.ratio);
                    }
                }
            },
            ready: function (e) {
                $image.cropper('zoomTo', 1);
            }
        });

    });

    $('#zoom-slider').slider({
        min: 0.01, max: 10,
        value: 1,
        step: 0.01, slide: function (event, ui) {
            if ($image.data('cropper')) {
                $image.cropper('zoomTo', ui.value);
            }
        }
    });

    $('#modal').on('hidden.bs.modal', function () {
        $image.cropper('destroy');
        //        $('#image-selector').val("");
        $('#zoom-slider').slider("value", 1);
    });

    document.getElementById('upload_image').addEventListener('click', function (e) {
        e.preventDefault();
        var formData = new FormData();
        var canvasImage;
        if (fileType == "image/png") {
            canvasImage = $image.cropper('getCroppedCanvas', {
                width: 450,
                height: 530
            });
        } else {
            canvasImage = $image.cropper('getCroppedCanvas', {
                fillColor: '#fff',
                width: 450,
                height: 530
            });
        }

        var croppedImage = canvasImage.toDataURL(fileType);
        avatar.src = canvasImage.toDataURL();
        $("#image_src").val(canvasImage.toDataURL());
        formData.append('croppie', croppedImage);
        formData.append('action', "uploadimage");

        $.ajax({
            type: 'GET',
            url: $('#form').attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log("success");
                console.log(data);
                $('#modal').modal('hide');
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });

</script>
@endsection