<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model {

    protected $table = "seo";
    protected $primaryKey = "seo_id";
    protected $fillable = ['org_id', 'site_name', 'site_title', 'site_description', 'site_keywords', 'site_url', 'google_analytic', 'created_by', 'created_at', 'updated_by', 'updated_at'];

}
