<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public static function getOrgId() {
        $orgId = NULL;
        $code = 'ce';
        $org = DB::table('org')->where('code', $code)->first();
        if ($org != NULL) {
            $orgId = $org->org_id;
        }
        return $orgId;
    }

    public static function getOrg($orgId) {
        return DB::table('org')->where('org_id', $orgId)->first();
    }

    public static function datetimeNow() {
        return date("Y-m-d H:i:s");
    }

    public static function getUsername() {
        return Auth::user()->username;
    }

    public static function formatedDateTime($date) {
        return date("d M Y H:i:s", strtotime($date));
    }

    public static function getRequest() {
        return array_merge($_POST, $_GET);
    }

}
