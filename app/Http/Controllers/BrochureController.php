<?php

namespace App\Http\Controllers;

use App\Brochure;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Imagick;

class BrochureController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $brochure = DB::table('brochure')->get();
        return view('brochure.index', ['brochure' => $brochure]);
    }

    public function form($id = NULL) {
        $orgId = Controller::getOrgId();
        $brochureId = NULL;
        $name = NULL;
        $fileUrl = NULL;
        $fileName = NULL;
        $typeFile = NULL;
        $statusTitle = 'Tambah Brochure';
        $statusIcon = 'fa fa-plus';

        if (strlen($id) > 0) {
            $brochure = DB::table('brochure')->where('brochure_id', $id)->where('org_id', $orgId)->first();
            if ($brochure != NULL) {
                $brochureId = $brochure->brochure_id;
                $name = $brochure->name;
                $typeFile = $brochure->type_file;
                $fileUrl = $brochure->file_url;
                $fileName = $brochure->file_name;
                $statusTitle = 'Ubah Brochure';
                $statusIcon = 'fa fa-pencil';
            } else {
                return redirect('/brochure')->with('error', 'Brochure tidak ditemukan !');
            }
        }

        return view('brochure.form', [
            'brochure_id' => $brochureId,
            'name' => $name,
            'type_file' => $typeFile,
            'file_url' => $fileUrl,
            'file_name' => $fileName,
            'status_title' => $statusTitle,
            'status_icon' => $statusIcon,
        ]);
    }

    public function save(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'image_src' => 'Gambar',
            'name' => 'Nama Brochure',
        ];
        $this->validate($request, [
            'file_pdf' => '',
            'name' => 'required',
            'image_src' => '',
                ], $messages, $attrbName);


        $orgId = Controller::getOrgId();
        $org = Controller::getOrg($orgId);
        $brochureId = $request['brochure_id'] ? : NULL;
        $name = $request['name'] ? : NULL;
        $typeFile = $request['type_file'] ? : NULL;
        $imageSrc = $request['image_src'] ? : NULL;
        $fileUrl = NULL;
        $fileName = NULL;
        $coverFilename = NULL;
        $coverFileUrl = NULL;
        $err = 0;
        if (strlen($brochureId) > 0) {
            $errMessage = 'Brochure berhasil di ubah !';
        } else {
            $errMessage = 'Brochure berhasil di tambahkan !';
        }

        if (strlen($brochureId) > 0) {
            $checkBrochure = DB::table('brochure')->select(['file_url', 'file_name'])->where('brochure_id', $brochureId)->first();
            if ($checkBrochure) {
                $fileUrl = $checkBrochure->file_url;
                $fileName = $checkBrochure->file_name;
            } else {
                $err++;
                $errMessage = 'Brochure tidak ditemukan !';
            }
        }

        if ($err == 0) {
            try {
                if (!file_exists('file')) {
                    mkdir('file', 0777, true);
                }
                if (!file_exists('file/brochure/')) {
                    mkdir('file/brochure/', 0777, true);
                }

                if (strlen($imageSrc) > 0 && $typeFile == 'image') {
                    if (strlen($brochureId) > 0) {
                        if ($imageSrc != $fileName) {
                            $imagePath = "file/brochure/$fileName";
                            if (file_exists($imagePath)) {
                                File::delete($imagePath);
                            }
                            $data = self::uploadImage($imageSrc);
                            $fileUrl = $data['file_url'];
                            $fileName = $data['file_name'];
                        }
                    } else {
                        $data = self::uploadImage($imageSrc);
                        $fileUrl = $data['file_url'];
                        $fileName = $data['file_name'];
                    }
                } else {
                    if (strlen($brochureId) > 0) {
                        $filePath = "file/brochure/$fileName";
                        if (file_exists($filePath)) {
                            File::delete($filePath);
                        }
                    }
                    if ($request->has('file_pdf')) {
                        $file = $request->file('file_pdf');
                        $nameFile = time();
                        $fileName = $nameFile . '.' . $file->getClientOriginalExtension();
                        $folder = 'file/brochure/' . $fileName;
                        copy($_FILES['file_pdf']['tmp_name'], $folder);
                        $fileUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/' . $folder;

                        //create cover image
                        $replace = "https://" . $_SERVER['SERVER_NAME'] . "/";
                        $source = $fileUrl;
                        $sourceReplace = str_replace($replace, "", $source);
                        $coverFilename = 'cover_' . str_replace('.pdf', '', $fileName) . '.png';
                        $folder = 'file/brochure/' . $coverFilename;
                        $coverFileUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/' . $folder;
                        self::genPdfThumbnail($sourceReplace, $coverFilename);
                    }
                }

                $brochure = Brochure::firstOrNew(['brochure_id' => $brochureId]);
                $brochure->org_id = $orgId;
                $brochure->name = $name;
                $brochure->type_file = $typeFile;
                $brochure->file_name = $fileName;
                $brochure->file_url = $fileUrl;
                $brochure->cover_pdf_name = $coverFilename;
                $brochure->cover_pdf_url = $coverFileUrl;

                if (strlen($brochureId) > 0) {
                    $brochure->updated_by = Controller::getUsername();
                } else {
                    $brochure->created_by = Controller::getUsername();
                    $brochure->updated_by = Controller::getUsername();
                }
                $brochure->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/brochure')->with('success', $errMessage);
        } else {
            return redirect('/brochure/add')->with('error', $errMessage);
        }
    }

    public function delete($id = NULL) {
        $err = 0;
        $errMessage = 'Brochure berhasil di hapus !';

        if (strlen($id) == 0) {
            $err++;
            $errMessage = 'Id tidak ada !';
        }

        if ($err == 0) {
            try {
                $brochure = DB::table('brochure')->where('brochure_id', $id)->first();
                $imageDelete = $brochure->file_name;
                $imagePath = "file/brochure/$imageDelete";
                if (file_exists($imagePath)) {
                    File::delete($imagePath);
                }
                if ($brochure->type_file == 'pdf') {
                    $coverDelete = $brochure->cover_pdf_name;
                    $coverPath = "file/brochure/$coverDelete";
                    if (file_exists($coverPath)) {
                        File::delete($coverPath);
                    }
                }
                DB::table('brochure')->where('brochure_id', $id)->delete();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/brochure')->with('success', $errMessage);
        } else {
            return redirect('/brochure')->with('error', $errMessage);
        }
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $fileName = time() . '.' . 'png';
        $fileImage = 'file/brochure/' . $fileName;
        $temp = 'file/brochure/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(765, 1080);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 765, 1080, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $fileUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('file_name' => $fileName, 'file_url' => $fileUrl);
    }

    public function generateCoverPdf() {
        $brochure = DB::table('brochure')->get();
        $orgId = Controller::getOrgId();
        $org = Controller::getOrg($orgId);
        foreach ($brochure as $row => $value) {
            if ($value->type_file == 'pdf') {
                $replace = "https://" . $_SERVER['SERVER_NAME'] . "/";
                $source = $value->file_url;
                $sourceReplace = str_replace($replace, "", $source);
                $filename = 'cover_' . str_replace('.pdf', '', $value->file_name) . '.png';
                $folder = 'file/brochure/' . $filename;
                $fileUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/' . $folder;
                self::genPdfThumbnail($sourceReplace, $filename);

                $brochureUpdate = Brochure::firstOrNew(['brochure_id' => $value->brochure_id]);
                $brochureUpdate->cover_pdf_url = $fileUrl;
                $brochureUpdate->cover_pdf_name = $filename;
                $brochureUpdate->save();
            }
        }
    }

    public static function genPdfThumbnail($source, $target) {

        if (!extension_loaded('imagick')) {
            echo 'imagick not installed';
        }
        if (file_exists($source)) {
            //$source = realpath($source);
            $target = dirname($source) . DIRECTORY_SEPARATOR . $target;
            $im = new Imagick($source . "[0]"); // 0-first page, 1-second page
            //$im->setImageColorspace(255); // prevent image colors from inverting
            $im->setimageformat("png");
            $im->thumbnailimage(765, 1080); // width and height
            $im->writeimage($target);
            $im->clear();
            $im->destroy();
        }
    }

}
