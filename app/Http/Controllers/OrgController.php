<?php

namespace App\Http\Controllers;

use App\Org;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrgController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $orgId = Controller::getOrgId();
        $domainBackend = NULL;
        $domainFrontend = NULL;
        $domainMail = NULL;
        $domainCpanel = NULL;
        $nameProject = NULL;
        $statusWeb = NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $fileName = NULL;
        $fileUrl = NULL;
        $org = DB::table('org')->where('org_id', $orgId)->first();
        if ($org != NULL) {
            $orgId = $org->org_id;
            $domainBackend = $org->domain_backend;
            $domainFrontend = $org->domain_frontend;
            $domainMail = $org->domain_mail;
            $domainCpanel = $org->domain_cpanel;
            $nameProject = $org->name;
            $websiteOn = $org->website_status;
            $imageUrl = $org->logo_url;
            $imageName = $org->logo_name;
            $fileName = $org->manual_book;
            $fileUrl = $org->manual_book_url;
            if ($websiteOn == '0') {
                $statusWeb = 'checked';
            } else {
                $statusWeb = '';
            }
        }
        return view('org.index', [
            'org_id' => $orgId,
            'domain_backend' => $domainBackend,
            'domain_frontend' => $domainFrontend,
            'domain_mail' => $domainMail,
            'domain_cpanel' => $domainCpanel,
            'name' => $nameProject,
            'statusWeb' => $statusWeb,
            'image_url' => $imageUrl,
            'image_name' => $imageName,
            'file_name' => $fileName,
            'file_url' => $fileUrl,
        ]);
    }

    public function update(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'domain_backend' => 'Domain Backend',
            'domain_frontend' => 'Domain Frontend',
            'name' => 'Nama Aplikasi',
            'image_src' => 'Mini Banner',
        ];
        $this->validate($request, [
            'domain_backend' => 'max:255|required',
            'domain_frontend' => 'max:255|required',
            'name' => 'max:255|required',
                ], $messages, $attrbName);

        $orgId = $request['org_id'] ?: NULL;
        $domainBackend = $request['domain_backend'] ?: NULL;
        $domainFrontend = $request['domain_frontend'] ?: NULL;
        $domainMail = $request['domain_mail'] ?: NULL;
        $domainCpanel = $request['domain_cpanel'] ?: NULL;
        $nameProject = $request['name'] ?: NULL;
        $statusButton = $request['switchButton'];
        $imageSrc = $request['image_src'] ?: NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $fileName = NULL;
        $filePath = NULL;
        $fileUrl = NULL;
        $err = 0;
        $errMessage = 'Setting berhasil di update !';

        if ($statusButton == 'on') {
            $statusWeb = '0';
        } else {
            $statusWeb = '1';
        }

        if (strlen($orgId) > 0) {
            $checkFile = DB::table('org')->select(['logo_url', 'logo_name', 'manual_book', 'manual_book_url'])->where('org_id', $orgId)->first();
            if ($checkFile) {
                $imageUrl = $checkFile->logo_url;
                $imageName = $checkFile->logo_name;
                $fileName = $checkFile->manual_book;
                $fileUrl = $checkFile->manual_book_url;
            }
        }

        $file = $request->file('file');
        if ($request->hasFile('file')) {
            //delete old file
            $fileOldPath = "manual_book/$fileName";
            if (file_exists($fileOldPath)) {
                File::delete($fileOldPath);
            }
            //upload new file
            $fileName = $file->getClientOriginalName();
            $filePath = "manual_book";
            $file->move($filePath, $fileName);
            $fileUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $filePath . '/' . $fileName;
        }

        if ($err == 0) {
            try {
                if (strlen($imageSrc) > 0) {
                    if (!file_exists('img')) {
                        mkdir('img', 0777, true);
                    }
                    if (!file_exists('img/org/')) {
                        mkdir('img/org/', 0777, true);
                    }
                    if (strlen($orgId) > 0) {
                        if ($imageSrc != $imageName) {
                            $imagePath = "img/org/$imageName";
                            if (file_exists($imagePath)) {
                                File::delete($imagePath);
                            }
                            $data = self::uploadImage($imageSrc);
                            $imageUrl = $data['image_url'];
                            $imageName = $data['image_name'];
                        }
                    } else {
                        $data = self::uploadImage($imageSrc);
                        $imageUrl = $data['image_url'];
                        $imageName = $data['image_name'];
                    }
                }




                $org = Org::firstOrNew(['org_id' => $orgId]);
                $org->domain_backend = $domainBackend;
                $org->domain_frontend = $domainFrontend;
                $org->domain_mail = $domainMail;
                $org->domain_cpanel = $domainCpanel;
                $org->name = $nameProject;
                $org->website_status = $statusWeb;
                $org->logo_name = $imageName;
                $org->logo_url = $imageUrl;
                $org->manual_book = $fileName;
                $org->manual_book_url = $fileUrl;
                $org->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/org')->with('success', $errMessage);
        } else {
            return redirect('/org')->with('error', $errMessage);
        }
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $imageName = time() . '.' . 'png';
        $fileImage = 'img/org/' . $imageName;
        $temp = 'img/org/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(2274, 600);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 2274, 600, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('image_name' => $imageName, 'image_url' => $imageUrl);
    }

}
