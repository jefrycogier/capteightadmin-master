<?php

namespace App\Http\Controllers;

use App\About;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $orgId = Controller::getOrgId();
        $aboutId = NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $description = NULL;
        $about = DB::table('about')->where('org_id', $orgId)->first();
        if ($about != NULL) {
            $aboutId = $about->about_id;
            $imageUrl = $about->image_url;
            $imageName = $about->image_name;
            $description = $about->description;
        }
        return view('about.index', [
            'about_id' => $aboutId,
            'image_url' => $imageUrl,
            'image_name' => $imageName,
            'description' => $description,
        ]);
    }

    public function update(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'image_src' => 'Gambar',
            'description' => 'Deskripsi About',
            'description_image' => 'Deskripsi Dengan Gambar',
            'title_image' => 'Judul Dengan Gambar',
        ];
        $this->validate($request, [
            'image_src' => '',
            'description' => '',
            'description_image' => 'max:500',
            'title_image' => 'max:50',
                ], $messages, $attrbName);

        $orgId = Controller::getOrgId();
        $aboutId = $request['about_id'] ? : NULL;
        $imageSrc = $request['image_src'] ? : NULL;
        $description = $request['description'] ? : NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $err = 0;
        $errMessage = 'Data about berhasil di update !';

        if (strlen($aboutId) > 0) {
            $checkAbout = DB::table('about')->select(['image_url', 'image_name'])->where('about_id', $aboutId)->first();
            if ($checkAbout) {
                $imageUrl = $checkAbout->image_url;
                $imageName = $checkAbout->image_name;
            } else {
                $err++;
                $errMessage = 'About tidak ditemukan !';
            }
        }

        if ($err == 0) {
            try {
                if (strlen($imageSrc) > 0) {
                    if (!file_exists('img')) {
                        mkdir('img', 0777, true);
                    }
                    if (!file_exists('img/about/')) {
                        mkdir('img/about/', 0777, true);
                    }
                    if (strlen($aboutId) > 0) {
                        if ($imageSrc != $imageName) {
                            $imagePath = "img/about/$imageName";
                            if (file_exists($imagePath)) {
                                File::delete($imagePath);
                            }
                            $data = self::uploadImage($imageSrc);
                            $imageUrl = $data['image_url'];
                            $imageName = $data['image_name'];
                        }
                    } else {
                        $data = self::uploadImage($imageSrc);
                        $imageUrl = $data['image_url'];
                        $imageName = $data['image_name'];
                    }
                }

                $about = About::firstOrNew(['org_id' => $orgId, 'about_id' => $aboutId]);
                $about->org_id = $orgId;
                $about->description = $description;
                $about->image_url = $imageUrl;
                $about->image_name = $imageName;
                if (strlen($aboutId) > 0) {
                    $about->updated_by = Controller::getUsername();
                } else {
                    $about->created_by = Controller::getUsername();
                    $about->updated_by = Controller::getUsername();
                }
                $about->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/about')->with('success', $errMessage);
        } else {
            return redirect('/about')->with('error', $errMessage);
        }
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $imageName = time() . '.' . 'png';
        $fileImage = 'img/about/' . $imageName;
        $temp = 'img/about/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(450, 530);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 450, 530, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('image_name' => $imageName, 'image_url' => $imageUrl);
    }

}
