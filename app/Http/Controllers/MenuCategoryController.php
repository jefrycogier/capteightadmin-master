<?php

namespace App\Http\Controllers;

use App\MenuCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;

class MenuCategoryController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($error = 0, $errMessage = '') {
        $orgId = Controller::getOrgId();
        $categories = MenuCategory::where(['org_id' => $orgId])->get();
        $canAdd = true;
        if (count($categories) == 2) {
            $canAdd = false;
        }
        $categoriesTree = '';
        $special = "'";
        foreach ($categories as $cat) {
            $categoriesTree .= '<li class="dd-item" data-id=' . $cat->menu_category_id . '>';
            if (strlen($cat->image_url) > 0) {
                $categoriesTree .= '<div class="dd-handle"><img src="' . $cat->image_url . '" height="auto" width="30">&nbsp' . $cat->name;
            } else {
                $categoriesTree .= '<div class="dd-handle">' . $cat->name;
            }
            $categoriesTree .= '<div class="btn-group pull-right" style="margin-top: 5px;">';
            $categoriesTree .= '<a href="/menu_category/edit/' . $cat->menu_category_id . '" style="margin-right: 5px;" type="button" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>';
            $categoriesTree .= '<a href="/menu_category/delete/' . $cat->menu_category_id . '" onclick="return confirm(' . $special . 'Apakah anda yakin ?' . $special . ')" type="button" id="btn-confirm" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>';
            $categoriesTree .= '</div>';
            $categoriesTree .= '</div>';
            $categoriesTree .= '</ol>';
            $categoriesTree .= '</li>';
        }

        return view('menu_category.index', [
            'error' => $error,
            'errorMessage' => $errMessage,
            'categoriesTree' => $categoriesTree,
            'can_add' => $canAdd,
        ]);
    }

    public function form($id = NULL) {
        $orgId = Controller::getOrgId();
        $categoryId = NULL;
        $categoryName = NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $statusTitle = 'Tambah Jenis Menu';
        $statusIcon = 'fa fa-plus';

        if (strlen($id) > 0) {
            $category = DB::table('menu_category')->where('menu_category_id', $id)->first();
            if ($category != NULL) {
                $categoryId = $category->menu_category_id;
                $categoryName = $category->name;
                $imageUrl = $category->image_url;
                $imageName = $category->image_name;
                $statusTitle = 'Ubah Jenis Menu';
                $statusIcon = 'fa fa-pencil';
            } else {
                return redirect('/menu_category')->with('error', 'Jenis Menu tidak ditemukan !');
            }
        }

        return view('menu_category.form', [
            'menu_category_id' => $categoryId,
            'category_name' => $categoryName,
            'image_url' => $imageUrl,
            'image_name' => $imageName,
            'status_title' => $statusTitle,
            'status_icon' => $statusIcon,
        ]);
    }

    public function save(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'image_src' => 'Gambar',
            'category_name' => 'Nama Jenis Menu',
        ];
        $this->validate($request, [
            'category_name' => 'required|max:255',
                ], $messages, $attrbName);

        $orgId = Controller::getOrgId();
        $categoryId = $request['menu_category_id'] ? : NULL;
        $name = $request['category_name'] ? : NULL;
        $imageSrc = $request['image_src'] ? : NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $err = 0;
        if (strlen($categoryId) > 0) {
            $errMessage = 'Jenis Menu berhasil di ubah !';
        } else {
            $errMessage = 'Jenis Menu berhasil di tambahkan !';
        }

        if ($err == 0) {
            try {
                if (strlen($imageSrc) > 0) {
                    if (!file_exists('img')) {
                        mkdir('img', 0777, true);
                    }
                    if (!file_exists('img/category/')) {
                        mkdir('img/category/', 0777, true);
                    }
                    if (strlen($categoryId) > 0) {
                        if ($imageSrc != $imageName) {
                            $imagePath = "img/category/$imageName";
                            if (file_exists($imagePath)) {
                                File::delete($imagePath);
                            }
                            $data = self::uploadImage($imageSrc);
                            $imageUrl = $data['image_url'];
                            $imageName = $data['image_name'];
                        }
                    } else {
                        $data = self::uploadImage($imageSrc);
                        $imageUrl = $data['image_url'];
                        $imageName = $data['image_name'];
                    }
                }


                $category = MenuCategory::firstOrNew(['menu_category_id' => $categoryId]);
                $category->org_id = $orgId;
                $category->name = $name;
                $category->image_name = $imageName;
                $category->image_url = $imageUrl;
                if (strlen($categoryId) > 0) {
                    $category->updated_by = Controller::getUsername();
                } else {
                    $category->created_by = Controller::getUsername();
                    $category->updated_by = Controller::getUsername();
                }
                $category->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/menu_category')->with('success', $errMessage);
        } else {
            return redirect('/menu_category/add')->with('error', $errMessage);
        }
    }

    public function delete($id) {
        $err = 0;
        $errMessage = 'Jenis Menu berhasil di hapus !';

        if (strlen($id) == 0) {
            $err++;
            $errMessage = 'Id tidak ada !';
        }

        $menu = DB::table('menu')->where('menu_category_id', $id)->first();
        if ($menu != NULL) {
            $err++;
            $errMessage = 'Jenis menu masih digunakan di gunakan di master menu. Harap hapus semua menu yang menggunakan jenis menu ini. ';
        }

        if ($err == 0) {
            try {
                $category = DB::table('menu_category')->where('menu_category_id', $id)->first();
                $imageDelete = $category->image_name;
                $imagePath = "img/category/$imageDelete";
                if (file_exists($imagePath)) {
                    File::delete($imagePath);
                }
                DB::table('menu_category')->where('menu_category_id', $id)->delete();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/menu_category')->with('success', $errMessage);
        } else {
            return redirect('/menu_category')->with('error', $errMessage);
        }
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $imageName = time() . '.' . 'png';
        $fileImage = 'img/category/' . $imageName;
        $temp = 'img/category/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(300, 300);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 300, 300, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('image_name' => $imageName, 'image_url' => $imageUrl);
    }

}
