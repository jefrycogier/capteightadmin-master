<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Location;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $contact = DB::table('contact')
                ->select(['contact.*', 'location.name as location_name'])
                ->leftJoin('location', 'location.location_id', '=', 'contact.location_id')
                ->get();
        return view('contact.index', ['contact' => $contact]);
    }

    public function form($id = NULL) {
        $orgId = Controller::getOrgId();
        $contactId = NULL;
        $address = NULL;
        $locationId = NULL;
        $statusTitle = 'Tambah Lokasi';
        $statusIcon = 'fa fa-plus';
        $locationList = Location::where(['hapus' => 'T'])->get();

        if (strlen($id) > 0) {
            $contact = DB::table('contact')->where('contact_id', $id)->where('org_id', $orgId)->first();
            if ($contact != NULL) {
                $contactId = $contact->contact_id;
                $locationId = $contact->location_id;
                $address = $contact->address;
                $statusTitle = 'Ubah Menu';
                $statusIcon = 'fa fa-pencil';
            } else {
                return redirect('/contact')->with('error', 'Lokasi tidak ditemukan !');
            }
        }

        return view('contact.form', [
            'contact_id' => $contactId,
            'location_id' => $locationId,
            'location_list' => $locationList,
            'address' => $address,
            'status_title' => $statusTitle,
            'status_icon' => $statusIcon,
        ]);
    }

    public function save(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'address' => 'Alamat',
            'location_id' => 'Lokasi',
        ];
        $this->validate($request, [
            'address' => 'max:500|required',
            'location_id' => 'required',
                ], $messages, $attrbName);

        $orgId = Controller::getOrgId();
        $contactId = $request['contact_id'] ? : NULL;
        $address = $request['address'] ? : NULL;
        $locationId = $request['location_id'] ? : NULL;
        $err = 0;
        if (strlen($contactId) > 0) {
            $errMessage = 'Lokasi berhasil di ubah !';
        } else {
            $errMessage = 'Lokasi berhasil di tambahkan !';
        }

        if ($err == 0) {
            try {
                $contact = Contact::firstOrNew(['contact_id' => $contactId]);
                $contact->org_id = $orgId;
                $contact->address = $address;
                $contact->location_id = $locationId;
                if (strlen($contactId) > 0) {
                    $contact->updated_by = Controller::getUsername();
                } else {
                    $contact->created_by = Controller::getUsername();
                    $contact->updated_by = Controller::getUsername();
                }
                $contact->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/contact')->with('success', $errMessage);
        } else {
            return redirect('/contact/add')->with('error', $errMessage);
        }
    }

    public function delete($id = NULL) {
        $err = 0;
        $errMessage = 'Lokasi berhasil di hapus !';

        if (strlen($id) == 0) {
            $err++;
            $errMessage = 'Id tidak ada !';
        }

        if ($err == 0) {
            try {
                DB::table('contact')->where('contact_id', $id)->delete();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/contact')->with('success', $errMessage);
        } else {
            return redirect('/contact')->with('error', $errMessage);
        }
    }

}
