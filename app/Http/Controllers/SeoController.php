<?php

namespace App\Http\Controllers;

use App\Seo;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SeoController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $orgId = Controller::getOrgId();
        $seoId = NULL;
        $siteName = NULL;
        $siteDescription = NULL;
        $siteKeywords = NULL;
        $siteUrl = NULL;
        $googleAnalytic = NULL;
        $seo = DB::table('seo')->where('org_id', $orgId)->first();
        if ($seo != NULL) {
            $orgId = $seo->org_id;
            $seoId = $seo->seo_id;
            $siteName = $seo->site_name;
            $siteDescription = $seo->site_description;
            $siteKeywords = $seo->site_keywords;
            $siteUrl = $seo->site_url;
            $googleAnalytic = $seo->google_analytic;
        }
        return view('seo.index', [
            'org_id' => $orgId,
            'seo_id' => $seoId,
            'site_name' => $siteName,
            'site_description' => $siteDescription,
            'site_keywords' => $siteKeywords,
            'site_url' => $siteUrl,
            'google_analytic' => $googleAnalytic,
        ]);
    }

    public function update(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'site_name' => 'Site Name',
            'site_description' => 'Site Description',
            'site_keywords' => 'Site Keywords',
            'site_url' => 'Site Url',
            'google_analytic' => 'Google Analytic',
        ];
        $this->validate($request, [
            'site_name' => 'max:500',
            'site_description' => 'max:500',
            'site_keywords' => 'max:500',
            'site_url' => 'max:500',
            'google_analytic' => '',
                ], $messages, $attrbName);

        $orgId = Controller::getOrgId();
        $seoId = $request['seo_id'] ?: NULL;
        $siteName = $request['site_name'] ?: NULL;
        $siteDescription = $request['site_description'] ?: NULL;
        $siteKeywords = $request['site_keywords'] ?: NULL;
        $siteUrl = $request['site_url'] ?: NULL;
        $googleAnalytic = $request['google_analytic'] ?: NULL;
        $err = 0;
        $errMessage = 'Seo berhasil di update !';

        if ($err == 0) {
            try {
                $seo = Seo::firstOrNew(['seo_id' => $seoId]);
                $seo->site_name = $siteName;
                $seo->org_id = $orgId;
                $seo->site_description = $siteDescription;
                $seo->site_keywords = $siteKeywords;
                $seo->site_url = $siteUrl;
                $seo->google_analytic = $googleAnalytic;
                $seo->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/seo')->with('success', $errMessage);
        } else {
            return redirect('/seo')->with('error', $errMessage);
        }
    }

}
