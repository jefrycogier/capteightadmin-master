<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller {

    use RegistersUsers;

    protected $redirectTo = '/users';

    public function __construct() {
        $this->middleware('auth');
    }

    protected function validator(array $data) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
            'unique' => ':attribute sudah dipakai !',
            'confirmed' => ':attribute tidak sama !',
        ];
        $attrbName = [
            'image_src' => 'Gambar',
            'username' => 'Username',
            'name' => 'Nama',
            'roles' => 'Jabatan',
            'email' => 'Email',
            'password' => 'Password',
        ];
        return Validator::make($data, [
                    'image_src' => 'required',
                    'name' => 'required|string|max:255',
                    'username' => 'required|string|max:255|unique:users',
                    'roles' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
                        ], $messages, $attrbName);
    }

    protected function create(array $data) {
        $orgId = Controller::getOrgId();

        if (!file_exists('img')) {
            mkdir('img', 0777, true);
        }
        if (!file_exists('img/users/')) {
            mkdir('img/users/', 0777, true);
        }
        $dataImage = self::uploadImage($data['image_src']);

        return User::create([
                    'org_id' => $orgId,
                    'name' => $data['name'],
                    'username' => $data['username'],
                    'roles' => $data['roles'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'image_url' => $dataImage['image_url'],
                    'image_name' => $dataImage['image_name'],
                    'created_by' => Controller::getUsername(),
                    'updated_by' => Controller::getUsername(),
        ]);
    }

    public function register(Request $request) {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user) ?: redirect($this->redirectPath())->with('success', 'User berhasil di tambahkan !');
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $imageName = time() . '.' . 'png';
        $fileImage = 'img/users/' . $imageName;
        $temp = 'img/users/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(500, 500);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 500, 500, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('image_name' => $imageName, 'image_url' => $imageUrl);
    }

}
