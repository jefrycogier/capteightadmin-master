<?php

namespace App\Http\Controllers;

use App\User;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $orgId = Controller::getOrgId();
        $users = DB::table('users')->where('org_id', $orgId)->where('is_admin', '0')->get();
        return view('users.index', ['users' => $users]);
    }

    public function form($id = NULL) {
        $orgId = Controller::getOrgId();
        $userId = NULL;
        $name = NULL;
        $username = NULL;
        $roles = NULL;
        $email = NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $statusTitle = 'Tambah User';
        $statusIcon = 'fa fa-plus';
        $readOnly = false;
        $formAction = route('register');

        if (strlen($id) > 0) {
            $package = DB::table('users')->where('id', $id)->where('org_id', $orgId)->where('is_admin', '0')->first();
            if ($package != NULL) {
                $userId = $package->id;
                $name = $package->name;
                $username = $package->username;
                $roles = $package->roles;
                $email = $package->email;
                $imageUrl = $package->image_url;
                $imageName = $package->image_name;
                $statusTitle = 'Ubah User';
                $statusIcon = 'fa fa-pencil';
                $readOnly = true;
                $formAction = '/users/save';
            } else {
                return redirect('/users')->with('error', 'User tidak ditemukan !');
            }
        }

        return view('users.form', [
            'user_id' => $userId,
            'name' => $name,
            'username' => $username,
            'roles' => $roles,
            'email' => $email,
            'image_url' => $imageUrl,
            'image_name' => $imageName,
            'status_title' => $statusTitle,
            'status_icon' => $statusIcon,
            'readonly' => $readOnly,
            'form_action' => $formAction,
        ]);
    }

    public function save(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'image_src' => 'Gambar',
            'username' => 'Username',
            'name' => 'Nama',
            'roles' => 'Jabatan',
            'email' => 'Email',
        ];
        $this->validate($request, [
            'image_src' => 'required',
            'username' => 'required|max:255|unique:users,username,' . $request['user_id'],
            'name' => 'required|max:255',
            'roles' => 'required|max:255',
            'email' => 'required|max:255|unique:users,email,' . $request['user_id'],
                ], $messages, $attrbName);

        $orgId = Controller::getOrgId();
        $userId = $request['user_id'] ?: NULL;
        $name = $request['name'] ?: NULL;
        $roles = $request['roles'] ?: NULL;
        $email = $request['email'] ?: NULL;
        $imageSrc = $request['image_src'] ?: NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $err = 0;
        $errMessage = 'User berhasil di ubah !';

        if (strlen($userId) == 0) {
            $err++;
            $errMessage = 'User id kosong !';
        }

        if (strlen($userId) > 0) {
            $checkUser = DB::table('users')->select(['image_url', 'image_name'])->where('id', $userId)->first();
            if ($checkUser) {
                $imageUrl = $checkUser->image_url;
                $imageName = $checkUser->image_name;
            } else {
                $err++;
                $errMessage = 'User tidak ditemukan !';
            }
        }

        if ($err == 0) {
            try {
                if (strlen($imageSrc) > 0) {
                    if (!file_exists('img')) {
                        mkdir('img', 0777, true);
                    }
                    if (!file_exists('img/users/')) {
                        mkdir('img/users/', 0777, true);
                    }
                    if (strlen($userId) > 0) {
                        if ($imageSrc != $imageName) {
                            $imagePath = "img/users/$imageName";
                            if (file_exists($imagePath)) {
                                File::delete($imagePath);
                            }
                            $data = self::uploadImage($imageSrc);
                            $imageUrl = $data['image_url'];
                            $imageName = $data['image_name'];
                        }
                    }
                }

                $package = User::firstOrNew(['id' => $userId]);
                $package->org_id = $orgId;
                $package->name = $name;
                $package->roles = $roles;
                $package->email = $email;
                $package->image_name = $imageName;
                $package->image_url = $imageUrl;
                $package->updated_by = Controller::getUsername();
                $package->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/users')->with('success', $errMessage);
        } else {
            return redirect('/users/add')->with('error', $errMessage);
        }
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $imageName = time() . '.' . 'png';
        $fileImage = 'img/users/' . $imageName;
        $temp = 'img/users/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(500, 500);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 500, 500, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('image_name' => $imageName, 'image_url' => $imageUrl);
    }

    public function delete($id) {
        if ($id == Auth::user()->id) {
            return redirect('/users')->with('error', 'Tidak bisa menghapus user yang digunakan');
        }
        $users = DB::table('users')->where('id', $id)->first();
        $imageDelete = $users->image_name;
        $imagePath = "img/users/$imageDelete";
        if (file_exists($imagePath)) {
            File::delete($imagePath);
        }
        DB::table('users')->where('id', $id)->delete();
        return redirect('/users')->with('success', 'Hapus user berhasil !');
    }

    public function detail(Request $request) {
        $id = $request->id;
        $users = DB::table('users')
                        ->select('users.*')
                        ->where('id', $id)->first();
        $name = $users->name;
        $username = $users->username;
        $imageUrl = $users->image_url;
        $email = $users->email;
        $roles = $users->roles;
        $createdAt = $users->created_at;
        $createdBy = $users->created_by;
        $updatedAt = $users->updated_at;
        $updatedBy = $users->updated_by;
        return response()->json([
                    'name' => $name,
                    'username' => $username,
                    'image_url' => $imageUrl,
                    'email' => $email,
                    'roles' => $roles,
                    'created_at' => Controller::formatedDateTime($createdAt),
                    'created_by' => $createdBy,
                    'updated_at' => Controller::formatedDateTime($updatedAt),
                    'updated_by' => $updatedBy,
        ]);
    }

}
