<?php

namespace App\Http\Controllers;

use App\Menu;
use App\MenuCategory;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $menu = DB::table('menu')
                ->select(['menu.*', 'menu_category.name as category_name'])
                ->leftJoin('menu_category', 'menu_category.menu_category_id', '=', 'menu.menu_category_id')
                ->get();
        return view('menu.index', ['menu' => $menu]);
    }

    public function form($id = NULL) {
        $orgId = Controller::getOrgId();
        $menuId = NULL;
        $name = NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $menuCategoryId = NULL;
        $statusTitle = 'Tambah Menu';
        $statusIcon = 'fa fa-plus';
        $menuCategoryList = MenuCategory::where(['org_id' => $orgId])->get();

        if (strlen($id) > 0) {
            $menu = DB::table('menu')->where('menu_id', $id)->where('org_id', $orgId)->first();
            if ($menu != NULL) {
                $menuId = $menu->menu_id;
                $menuCategoryId = $menu->menu_category_id;
                $name = $menu->name;
                $imageUrl = $menu->image_url;
                $imageName = $menu->image_name;
                $statusTitle = 'Ubah Menu';
                $statusIcon = 'fa fa-pencil';
            } else {
                return redirect('/menu')->with('error', 'Menu tidak ditemukan !');
            }
        }

        return view('menu.form', [
            'menu_id' => $menuId,
            'menu_category_id' => $menuCategoryId,
            'menu_category_list' => $menuCategoryList,
            'name' => $name,
            'image_url' => $imageUrl,
            'image_name' => $imageName,
            'status_title' => $statusTitle,
            'status_icon' => $statusIcon,
        ]);
    }

    public function save(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'image_src' => 'Gambar',
            'name' => 'Nama menu',
            'menu_category_id' => 'Jenis Menu',
        ];
        $this->validate($request, [
            'name' => 'max:255|required',
            'menu_category_id' => 'required',
            'image_src' => 'required',
                ], $messages, $attrbName);

        $orgId = Controller::getOrgId();
        $menuId = $request['menu_id'] ? : NULL;
        $name = $request['name'] ? : NULL;
        $menuCategoryId = $request['menu_category_id'] ? : NULL;
        $imageSrc = $request['image_src'] ? : NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $err = 0;
        if (strlen($menuId) > 0) {
            $errMessage = 'Menu berhasil di ubah !';
        } else {
            $errMessage = 'Menu berhasil di tambahkan !';
        }

        if (strlen($menuId) > 0) {
            $checkMenu = DB::table('menu')->select(['image_url', 'image_name'])->where('menu_id', $menuId)->first();
            if ($checkMenu) {
                $imageUrl = $checkMenu->image_url;
                $imageName = $checkMenu->image_name;
            } else {
                $err++;
                $errMessage = 'Menu tidak ditemukan !';
            }
        }

        if ($err == 0) {
            try {
                if (strlen($imageSrc) > 0) {
                    if (!file_exists('img')) {
                        mkdir('img', 0777, true);
                    }
                    if (!file_exists('img/menu/')) {
                        mkdir('img/menu/', 0777, true);
                    }
                    if (strlen($menuId) > 0) {
                        if ($imageSrc != $imageName) {
                            $imagePath = "img/menu/$imageName";
                            if (file_exists($imagePath)) {
                                File::delete($imagePath);
                            }
                            $data = self::uploadImage($imageSrc);
                            $imageUrl = $data['image_url'];
                            $imageName = $data['image_name'];
                        }
                    } else {
                        $data = self::uploadImage($imageSrc);
                        $imageUrl = $data['image_url'];
                        $imageName = $data['image_name'];
                    }
                }

                $menu = Menu::firstOrNew(['menu_id' => $menuId]);
                $menu->org_id = $orgId;
                $menu->name = $name;
                $menu->menu_category_id = $menuCategoryId;
                $menu->image_name = $imageName;
                $menu->image_url = $imageUrl;
                if (strlen($menuId) > 0) {
                    $menu->updated_by = Controller::getUsername();
                } else {
                    $menu->created_by = Controller::getUsername();
                    $menu->updated_by = Controller::getUsername();
                }
                $menu->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/menu')->with('success', $errMessage);
        } else {
            return redirect('/menu/add')->with('error', $errMessage);
        }
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $imageName = time() . '.' . 'png';
        $fileImage = 'img/menu/' . $imageName;
        $temp = 'img/menu/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(500, 500);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 500, 500, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('image_name' => $imageName, 'image_url' => $imageUrl);
    }

    public function delete($id = NULL) {
        $err = 0;
        $errMessage = 'Menu berhasil di hapus !';

        if (strlen($id) == 0) {
            $err++;
            $errMessage = 'Id tidak ada !';
        }

        if ($err == 0) {
            try {
                $menu = DB::table('menu')->where('menu_id', $id)->first();
                $imageDelete = $menu->image_name;
                $imagePath = "img/menu/$imageDelete";
                if (file_exists($imagePath)) {
                    File::delete($imagePath);
                }
                DB::table('menu')->where('menu_id', $id)->delete();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/menu')->with('success', $errMessage);
        } else {
            return redirect('/menu')->with('error', $errMessage);
        }
    }

    public function detail(Request $request) {
        $id = $request->menu_id;
        $menu = DB::table('menu')
                ->select(['menu.*', 'menu_category.name as category_name'])
                ->leftJoin('menu_category', 'menu_category.menu_category_id', '=', 'menu.menu_category_id')
                ->where('menu_id', $id)
                ->first();

        $menuName = $menu->name;
        $menuImage = $menu->image_url;
        $menuCategory = $menu->category_name;
        $createdAt = $menu->created_at;
        $createdBy = $menu->created_by;
        $updatedAt = $menu->updated_at;
        $updatedBy = $menu->updated_by;

        return response()->json([
                    'menu_name' => $menuName,
                    'menu_category' => $menuCategory,
                    'menu_image' => $menuImage,
                    'created_at' => Controller::formatedDateTime($createdAt),
                    'created_by' => $createdBy,
                    'updated_at' => Controller::formatedDateTime($updatedAt),
                    'updated_by' => $updatedBy,
        ]);
    }

}
