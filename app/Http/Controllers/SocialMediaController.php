<?php

namespace App\Http\Controllers;

use App\SocialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SocialMediaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $orgId = Controller::getOrgId();
        $socialMediaId = NULL;
        $igUrl = NULL;
        $fbUrl = NULL;
        $twitterUrl = NULL;
        $wa = NULL;
        $about = DB::table('social_media')->where('org_id', $orgId)->first();
        if ($about != NULL) {
            $socialMediaId = $about->social_media_id;
            $igUrl = $about->instagram_url;
            $fbUrl = $about->facebook_url;
            $twitterUrl = $about->twitter_url;
            $wa = $about->whatsapp;
        }
        return view('social_media.index', [
            'social_media_id' => $socialMediaId,
            'url_ig' => $igUrl,
            'url_fb' => $fbUrl,
            'url_twitter' => $twitterUrl,
            'no_wa' => $wa,
        ]);
    }

    public function update(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'url_fb' => 'Link Facebook',
            'url_ig' => 'Link Instagram',
            'url_twitter' => 'Link Twitter',
            'no_wa' => 'No Whatsapp',
        ];
        $this->validate($request, [
            'url_fb' => '',
            'url_ig' => '',
            'url_twitter' => '',
            'no_wa' => 'max:255',
                ], $messages, $attrbName);

        $orgId = Controller::getOrgId();
        $socialMediaId = $request['social_media_id'] ?: NULL;
        $urlFb = $request['url_fb'] ?: NULL;
        $urlIg = $request['url_ig'] ?: NULL;
        $urlTwitter = $request['url_twitter'] ?: NULL;
        $noWa = $request['no_wa'] ?: NULL;
        $err = 0;
        $errMessage = 'Data social media berhasil di update !';

        if ($err == 0) {
            try {
                $socialMedia = SocialMedia::firstOrNew(['org_id' => $orgId, 'social_media_id' => $socialMediaId]);
                $socialMedia->org_id = $orgId;
                $socialMedia->facebook_url = $urlFb;
                $socialMedia->instagram_url = $urlIg;
                $socialMedia->twitter_url = $urlTwitter;
                $socialMedia->whatsapp = $noWa;
                if (strlen($socialMediaId) > 0) {
                    $socialMedia->updated_by = Controller::getUsername();
                } else {
                    $socialMedia->created_by = Controller::getUsername();
                    $socialMedia->updated_by = Controller::getUsername();
                }
                $socialMedia->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/social_media')->with('success', $errMessage);
        } else {
            return redirect('/social_media')->with('error', $errMessage);
        }
    }

}
