<?php

namespace App\Http\Controllers;

use App\Traits\UploadTrait;
use App\Banner;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller {

    use UploadTrait;

    public function __construct() {
        $this->middleware('auth');
        ini_set('memory_limit', '128M');
        ini_set('upload_max_filesize', '256M');
        ini_set('post_max_size', '256M');
        ini_set('max_execution_time', '600');
        ini_set('max_input_time', '600');
    }

    public function index() {
        $banner = DB::table('banner')->get();
        return view('banner.index', ['banner' => $banner]);
    }

    public function form($id = NULL) {
        $orgId = Controller::getOrgId();
        $bannerId = NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $statusTitle = 'Tambah Banner';
        $statusIcon = 'fa fa-plus';

        if (strlen($id) > 0) {
            $banner = DB::table('banner')->where('banner_id', $id)->where('org_id', $orgId)->first();
            if ($banner != NULL) {
                $bannerId = $banner->banner_id;
                $imageUrl = $banner->image_url;
                $imageName = $banner->image_name;
                $statusTitle = 'Ubah Banner';
                $statusIcon = 'fa fa-pencil';
            } else {
                return redirect('/banner')->with('error', 'Banner tidak ditemukan !');
            }
        }

        return view('banner.form', [
            'banner_id' => $bannerId,
            'image_url' => $imageUrl,
            'image_name' => $imageName,
            'status_title' => $statusTitle,
            'status_icon' => $statusIcon,
        ]);
    }

    public function save(Request $request) {
        $messages = [
            'required' => ':attribute harus diisi !',
            'min' => ':attribute minimal :min karakter !',
            'max' => ':attribute maksimal :max karakter !',
        ];
        $attrbName = [
            'image_src' => 'Gambar',
        ];
        $this->validate($request, [
            'image_src' => 'required',
                ], $messages, $attrbName);


        $orgId = Controller::getOrgId();
        $bannerId = $request['banner_id'] ?: NULL;
        $imageSrc = $request['image_src'] ?: NULL;
        $imageUrl = NULL;
        $imageName = NULL;
        $err = 0;
        if (strlen($bannerId) > 0) {
            $errMessage = 'Banner berhasil di ubah !';
        } else {
            $errMessage = 'Banner berhasil di tambahkan !';
        }

        if (strlen($bannerId) > 0) {
            $checkBanner = DB::table('banner')->select(['image_url', 'image_name'])->where('banner_id', $bannerId)->first();
            if ($checkBanner) {
                $imageUrl = $checkBanner->image_url;
                $imageName = $checkBanner->image_name;
            } else {
                $err++;
                $errMessage = 'Banner tidak ditemukan !';
            }
        }

        if ($err == 0) {
            try {
                if (strlen($imageSrc) > 0) {
                    if (!file_exists('img')) {
                        mkdir('img', 0777, true);
                    }
                    if (!file_exists('img/banner/')) {
                        mkdir('img/banner/', 0777, true);
                    }
                    if (strlen($bannerId) > 0) {
                        if ($imageSrc != $imageName) {
                            $imagePath = "img/banner/$imageName";
                            if (file_exists($imagePath)) {
                                File::delete($imagePath);
                            }
                            $data = self::uploadImage($imageSrc);
                            $imageUrl = $data['image_url'];
                            $imageName = $data['image_name'];
                        }
                    } else {
                        $data = self::uploadImage($imageSrc);
                        $imageUrl = $data['image_url'];
                        $imageName = $data['image_name'];
                    }
                }

                $banner = Banner::firstOrNew(['banner_id' => $bannerId]);
                $banner->org_id = $orgId;
                $banner->image_name = $imageName;
                $banner->image_url = $imageUrl;
                if (strlen($bannerId) > 0) {
                    $banner->updated_by = Controller::getUsername();
                } else {
                    $banner->created_by = Controller::getUsername();
                    $banner->updated_by = Controller::getUsername();
                }
                $banner->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/banner')->with('success', $errMessage);
        } else {
            return redirect('/banner/add')->with('error', $errMessage);
        }
    }

    public function save2(Request $request) {
        $request->validate([
            'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $orgId = Controller::getOrgId();
        $bannerId = $request->input('banner_id');

        $banner = Banner::firstOrNew(['banner_id' => $bannerId]);
        $banner->org_id = $orgId;
        if ($request->has('profile_image')) {
            $image = $request->file('profile_image');
            $name = time();
            $folder = '/img/banner/';
            $filePath = $name . '.' . $image->getClientOriginalExtension();
            $folder2 = 'img/banner/';
            $target_file = $folder2 . $filePath;

            list($width, $height) = getimagesize($_FILES["profile_image"]["tmp_name"]);
            $image2 = imagecreatetruecolor(2560, 1440);
            imagealphablending($image2, false);
            imagesavealpha($image2, true);
            imagecopyresampled($image2, imagecreatefromjpeg($_FILES["profile_image"]["tmp_name"]), 0, 0, 0, 0, 2560, 1440, $width, $height);
            imagepng($image2, $target_file);

            $banner->image_name = $filePath;
            $banner->image_url = $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . $folder . $name . '.' . $image->getClientOriginalExtension();
            if (strlen($bannerId) > 0) {
                $banner->updated_by = Controller::getUsername();
            } else {
                $banner->created_by = Controller::getUsername();
                $banner->updated_by = Controller::getUsername();
            }
        }

        $banner->save();

        return redirect('/banner')->with('success', 'Banner berhasil di tambahkan !');
    }

    public function delete($id = NULL) {
        $err = 0;
        $errMessage = 'Banner berhasil di hapus !';

        if (strlen($id) == 0) {
            $err++;
            $errMessage = 'Id tidak ada !';
        }

        if ($err == 0) {
            try {
                $banner = DB::table('banner')->where('banner_id', $id)->first();
                $imageDelete = $banner->image_name;
                $imagePath = "img/banner/$imageDelete";
                if (file_exists($imagePath)) {
                    File::delete($imagePath);
                }
                DB::table('banner')->where('banner_id', $id)->delete();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/banner')->with('success', $errMessage);
        } else {
            return redirect('/banner')->with('error', $errMessage);
        }
    }

    public static function uploadImage($imageSrc) {
        ob_start();
        $imageName = time() . '.' . 'png';
        $fileImage = 'img/banner/' . $imageName;
        $temp = 'img/banner/temp.png';
        $data = $imageSrc;
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($temp, $data);
        header('Content-Type: image/png');
        echo $type;
        switch ($type) {
            case 'data:image/jpeg':
                $imagetemp = imagecreatefromjpeg($temp);
                break;
            case 'data:image/gif':
                $imagetemp = imagecreatefromgif($temp);
                break;
            case 'data:image/png':
                $imagetemp = imagecreatefrompng($temp);
                break;
        }
        list($width, $height) = getimagesize($temp);
        $image = imagecreatetruecolor(2560, 1440);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopyresampled($image, $imagetemp, 0, 0, 0, 0, 2560, 1440, $width, $height);
        imagepng($image, $fileImage);
        unlink($temp);
        ob_end_clean();
        $imageUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $fileImage;

        return array('image_name' => $imageName, 'image_url' => $imageUrl);
    }

    public static function resizeImage($file) {

        define('MAX_WIDTH', 2560); //max image width               
        define('MAX_HEIGHT', 1440); //max image height 
        define('MAX_FILE_SIZE', 10485760);

        //iamge save path
        $path = 'img/banner/';

        //size of the resize image 
        $new_width = 128;
        $new_height = 128;

        //name of the new image           
        $nameOfFile = time();

        $image_type = $file['type'];
        $image_size = $file['size'];
        $image_error = $file['error'];
        $image_file = $file['tmp_name'];
        $image_name = $file['name'];

        $image_info = getimagesize($image_file);

        //check image type 
        if ($image_info['mime'] == 'image/jpeg' or $image_info['mime'] == 'image/jpg') {
            
        } else if ($image_info['mime'] == 'image/png') {
            
        } else if ($image_info['mime'] == 'image/gif') {
            
        } else {
            //set error invalid file type
        }

        if ($image_error) {
            //set error image upload error
        }

        if ($image_size > MAX_FILE_SIZE) {
            //set error image size invalid
        }

        switch ($image_info['mime']) {
            case 'image/jpg': //This isn't a valid mime type so we should probably remove it
            case 'image/jpeg':
                $image = imagecreatefromjpeg($image_file);
                break;
            case 'image/png':
                $image = imagecreatefrompng($image_file);
                break;
            case 'image/gif':
                $image = imagecreatefromgif($image_file);
                break;
        }

        if ($new_width == 0 && $new_height == 0) {
            $new_width = 100;
            $new_height = 100;
        }

        // ensure size limits can not be abused
        $new_width = min($new_width, MAX_WIDTH);
        $new_height = min($new_height, MAX_HEIGHT);

        //get original image h/w
        $width = imagesx($image);
        $height = imagesy($image);

        $align = 'b';
        $zoom_crop = 1;
        $origin_x = 0;
        $origin_y = 0;
        //TODO setting Memory
        // generate new w/h if not provided
        if ($new_width && !$new_height) {
            $new_height = floor($height * ($new_width / $width));
        } else if ($new_height && !$new_width) {
            $new_width = floor($width * ($new_height / $height));
        }

        // scale down and add borders
        if ($zoom_crop == 3) {

            $final_height = $height * ($new_width / $width);

            if ($final_height > $new_height) {
                $new_width = $width * ($new_height / $height);
            } else {
                $new_height = $final_height;
            }
        }

        // create a new true color image
        $canvas = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($canvas, false);

        $canvas_color = 'ffffff';
        if (strlen($canvas_color) < 6) {
            
        }

        $canvas_color_R = hexdec(substr($canvas_color, 0, 2));
        $canvas_color_G = hexdec(substr($canvas_color, 2, 2));
        $canvas_color_B = hexdec(substr($canvas_color, 2, 2));

        // Create a new transparent color for image
        $color = imagecolorallocatealpha($canvas, $canvas_color_R, $canvas_color_G, $canvas_color_B, 127);

        // Completely fill the background of the new image with allocated color.
        imagefill($canvas, 0, 0, $color);

        // scale down and add borders
        if ($zoom_crop == 2) {

            $final_height = $height * ($new_width / $width);

            if ($final_height > $new_height) {
                $origin_x = $new_width / 2;
                $new_width = $width * ($new_height / $height);
                $origin_x = round($origin_x - ($new_width / 2));
            } else {

                $origin_y = $new_height / 2;
                $new_height = $final_height;
                $origin_y = round($origin_y - ($new_height / 2));
            }
        }

        // Restore transparency blending
        imagesavealpha($canvas, true);

        if ($zoom_crop > 0) {

            $src_x = $src_y = 0;
            $src_w = $width;
            $src_h = $height;

            $cmp_x = $width / $new_width;
            $cmp_y = $height / $new_height;

            // calculate x or y coordinate and width or height of source
            if ($cmp_x > $cmp_y) {
                $src_w = round($width / $cmp_x * $cmp_y);
                $src_x = round(($width - ($width / $cmp_x * $cmp_y)) / 2);
            } else if ($cmp_y > $cmp_x) {
                $src_h = round($height / $cmp_y * $cmp_x);
                $src_y = round(($height - ($height / $cmp_y * $cmp_x)) / 2);
            }

            // positional cropping!
            if ($align) {
                if (strpos($align, 't') !== false) {
                    $src_y = 0;
                }
                if (strpos($align, 'b') !== false) {
                    $src_y = $height - $src_h;
                }
                if (strpos($align, 'l') !== false) {
                    $src_x = 0;
                }
                if (strpos($align, 'r') !== false) {
                    $src_x = $width - $src_w;
                }
            }

            // positional cropping!
            imagecopyresampled($canvas, $image, $origin_x, $origin_y, $src_x, $src_y, $new_width, $new_height, $src_w, $src_h);
        } else {
            imagecopyresampled($canvas, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        }
        //Straight from Wordpress core code. Reduces filesize by up to 70% for PNG's
        if ((IMAGETYPE_PNG == $image_info[2] || IMAGETYPE_GIF == $image_info[2]) && function_exists('imageistruecolor') && !imageistruecolor($image) && imagecolortransparent($image) > 0) {
            imagetruecolortopalette($canvas, false, imagecolorstotal($image));
        }
        $quality = 100;
        $nameOfFile = time();

        if (preg_match('/^image\/(?:jpg|jpeg)$/i', $image_info['mime'])) {
            imagejpeg($canvas, $path . $nameOfFile, $quality);
        } else if (preg_match('/^image\/png$/i', $image_info['mime'])) {
            imagepng($canvas, $path . $nameOfFile, floor($quality * 0.09));
        } else if (preg_match('/^image\/gif$/i', $image_info['mime'])) {
            imagegif($canvas, $path . $nameOfFile);
        }
    }

}
