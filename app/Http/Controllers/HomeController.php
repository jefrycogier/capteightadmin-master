<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $orgId = Controller::getOrgId();
        $org = Controller::getOrg($orgId);
        $domainMail = NULL;
        $domainCpanel = NULL;
        $manualBook = NULL;
        if (isset($org)) {
            $domainMail = $org->domain_mail;
            $domainCpanel = $org->domain_cpanel;
            $manualBook = $org->manual_book_url;
        }

        return view('home.index', [
            'domain_mail' => $domainMail,
            'domain_cpanel' => $domainCpanel,
            'manual_book' => $manualBook,
        ]);
    }

    public function changePassword() {
        return view('home.change_password');
    }

    public function updatePassword() {
        $err = 0;
        $errMessage = 'Password berhasil di ganti !';
        Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, \Auth::user()->password);
        });
        $messages = [
            'password' => 'Invalid current password.',
        ];
        $validator = Validator::make(request()->all(), [
                    'current_password' => 'required|password',
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required',
                        ], $messages);
        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator->errors());
        }

        if ($err == 0) {
            try {
                $user = User::find(Auth::id());
                $user->password = bcrypt(request('password'));
                $user->save();
            } catch (Exception $ex) {
                $err++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($err == 0) {
            return redirect('/change_password')->with('success', $errMessage);
        } else {
            return redirect('/change_password')->with('error', $errMessage);
        }
    }

}
