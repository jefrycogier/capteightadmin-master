<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model {

    protected $table = "banner";
    protected $primaryKey = 'banner_id';
    protected $fillable = ['org_id', 'image_name', 'image_url', 'created_by', 'created_at', 'updated_by', 'updated_at'];

}
