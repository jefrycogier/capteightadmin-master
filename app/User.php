<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'org_id', 'username', 'roles', 'image_name', 'image_url', 'created_at', 'created_by', 'updated_at', 'updated_by'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = true;

}
