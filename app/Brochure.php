<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brochure extends Model {

    protected $table = "brochure";
    protected $primaryKey = 'brochure_id';
    protected $fillable = ['org_id', 'name', 'type_file', 'file_name', 'file_url', 'cover_pdf_url', 'cover_pdf_name', 'created_by', 'created_at', 'updated_by', 'updated_at'];

}
