<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Org extends Model {

    protected $table = "org";
    protected $primaryKey = 'org_id';
    protected $fillable = ['name', 'domain_backend', 'domain_frontend', 'domain_mail', 'domain_cpanel', 'manual_book', 'website_status', 'logo_name', 'logo_url', 'favicon_name', 'favicon_url', 'created_at', 'updated_at'];

}
