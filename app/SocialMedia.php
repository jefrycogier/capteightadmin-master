<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model {

    protected $table = "social_media";
    protected $primaryKey = "social_media_id";
    protected $fillable = ['org_id', 'instagram_url', 'facebook_url', 'whatsapp', 'created_by', 'created_at', 'updated_by', 'updated_at'];

}
