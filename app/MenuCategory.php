<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuCategory extends Model {

    protected $table = "menu_category";
    protected $primaryKey = 'menu_category_id';
    protected $fillable = ['org_id', 'name', 'created_by', 'created_at', 'updated_by', 'updated_at'];

}
