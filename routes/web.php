<?php

Auth::routes();
//home
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/change_password', 'HomeController@changePassword');
Route::post('/update_password', 'HomeController@updatePassword');

//banner
Route::get('/banner', 'BannerController@index');
Route::get('/banner/add', 'BannerController@form');
Route::get('/banner/edit/{id}', 'BannerController@form');
Route::post('/banner/save', 'BannerController@save');
Route::get('/banner/delete/{id}', 'BannerController@delete');
//about
Route::get('/about', 'AboutController@index')->name('about');
Route::post('/about/save', 'AboutController@update');
//category
Route::get('/menu_category', 'MenuCategoryController@index');
Route::get('/menu_category/add', 'MenuCategoryController@form');
Route::get('/menu_category/edit/{id}', 'MenuCategoryController@form');
Route::post('/menu_category/save', 'MenuCategoryController@save');
Route::get('/menu_category/delete/{id}', 'MenuCategoryController@delete');
//menu
Route::get('/menu', 'MenuController@index');
Route::get('/menu/add', 'MenuController@form');
Route::get('/menu/edit/{id}', 'MenuController@form');
Route::post('/menu/save', 'MenuController@save');
Route::get('/menu/delete/{id}', 'MenuController@delete');
Route::post('/menu/detail', 'MenuController@detail');
//brochure
Route::get('/brochure', 'BrochureController@index');
Route::get('/brochure/add', 'BrochureController@form');
Route::get('/brochure/edit/{id}', 'BrochureController@form');
Route::post('/brochure/save', 'BrochureController@save');
Route::get('/brochure/delete/{id}', 'BrochureController@delete');
//contact
Route::get('/contact', 'ContactController@index');
Route::get('/contact/add', 'ContactController@form');
Route::get('/contact/edit/{id}', 'ContactController@form');
Route::post('/contact/save', 'ContactController@save');
Route::get('/contact/delete/{id}', 'ContactController@delete');
//social media
Route::get('/social_media', 'SocialMediaController@index');
Route::post('/social_media/save', 'SocialMediaController@update');
//users
Route::get('/users', 'UsersController@index');
Route::get('/users/add', 'UsersController@form');
Route::get('/users/edit/{id}', 'UsersController@form');
Route::post('/users/save', 'UsersController@save');
Route::get('/users/delete/{id}', 'UsersController@delete');
Route::post('/users/detail', 'UsersController@detail');
//ceo
Route::get('/seo', 'SeoController@index');
Route::post('/seo/save', 'SeoController@update');
//org
Route::get('/org', 'OrgController@index');
Route::post('/org/save', 'OrgController@update');
